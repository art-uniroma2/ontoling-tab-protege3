 /*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is OntoLing.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2004.
 * All Rights Reserved.
 *
 * OntoLing was developed by the Artificial Intelligence Research Group
 * (ai-nlp.info.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about OntoLing can be obtained at 
 * http//ai-nlp.info.uniroma2.it/software/OntoLing.html
 *
 */

 /*
  * Contributor(s): Armando Stellato stellato@info.uniroma2.it
 */

/*
 * GUI_Factory.java
 *
 * Created on 31 marzo 2004, 16.27
 */
package it.uniroma2.art.ontoling.protege.ui;

import javax.swing.JPanel;

import edu.stanford.smi.protege.model.*;

/**
 * @author  Armando Stellato
 */


public class GUI_Manager {

    private static Linguistic_GUI lGUI;

    private static OntologyPanel ontPanel;

    
    /** Creates a new instance of GUI_Factory */
    public GUI_Manager() {
        
    }
    
    public static void create_GUI(Project project)
    {
       lGUI = new Linguistic_GUI();
//     lGUI.setPreferredSize(new java.awt.Dimension(800, 600));
       lGUI.setSize(new java.awt.Dimension(800, 600));
       ontPanel = new OntologyPanel(project);
//     ontPanel.setPreferredSize(new java.awt.Dimension(800, 600));
       lGUI.MainSplitter.setRightComponent( ontPanel );//new ClsesPanel(project) );
    }
    
    public static Linguistic_GUI getLinguisticGUI()
    {
        return lGUI;
    }

    public static OntologyPanel getOntologyPanel()
    {
        return ontPanel;
    }

    
}
