 /*
  * The contents of this file are subject to the Mozilla Public License
  * Version 1.1 (the "License");  you may not use this file except in compliance
  * with the License.  You may obtain a copy of the License at
  * http//www.mozilla.org/MPL/
  *
  * Software distributed under the License is distributed on an "AS IS" basis,
  * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
  * the specific language governing rights and limitations under the License.
  *
  * The Original Code is ONTOLING_PROTEGE.
  *
  * The Initial Developer of the Original Code is University of Roma Tor Vergata.
  * Portions created by University of Roma Tor Vergata are Copyright (C) 2009.
  * All Rights Reserved.
  *
  * ONTOLING_PROTEGE was developed by the Artificial Intelligence Research Group
  * (ai-nlp.info.uniroma2.it) at the University of Roma Tor Vergata
  * Current information about ONTOLING_PROTEGE can be obtained at 
  * http//ai-nlp.info.uniroma2.it/software/...
  *
  */

  /*
   * Contributor(s): Armando Stellato stellato@info.uniroma2.it
  */
package it.uniroma2.art.ontoling.protege.ui;

import javax.swing.Action;
import javax.swing.JPopupMenu;
import javax.swing.JTree;

import edu.stanford.smi.protege.util.Disposable;
import edu.stanford.smi.protege.util.Selectable;

public interface OntoLingOntoTree extends Selectable, Disposable {

    Action addTerms(String label);
    Action addGloss(String label);
    Action addSenseIdToResource(String label);
    Action searchLinguisticResource(String label);
    Action changeNameToSelectedTerm(String label);
    Action createSubResourceWithSelectedTermAsID(String label);
    //Action callLinguisticEnrichment(String label);
    Action addSubResourcesUsingSubConceptsFromLinguisticResources(String label);
    
    JPopupMenu createStdPopupMenu();
    JPopupMenu getPopupMenu();
    void setPopupMenu(JPopupMenu menu);
    JTree getTree();
}
