 /*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is OntoLing.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2004.
 * All Rights Reserved.
 *
 * OntoLing was developed by the Artificial Intelligence Research Group
 * (ai-nlp.info.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about OntoLing can be obtained at 
 * http//ai-nlp.info.uniroma2.it/software/OntoLing.html
 *
 */

 /*
  * Contributor(s): Armando Stellato stellato@info.uniroma2.it
 */

/*
 * Linguistic_GUI.java
 *
 * Created on 11 marzo 2004, 10.27
 */

package it.uniroma2.art.ontoling.protege.ui;


import it.uniroma2.art.lw.model.objects.SearchFilter;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.HashMap;

import javax.swing.DefaultListModel;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JScrollPane;


/**
 *
 * @author  Armando Stellato <stellato@info.uniroma2.it>, Andrea Turbati <turbati@info.uniroma2.it>
 */
public class Linguistic_GUI extends JPanel {
    
	javax.swing.JComboBox CRelFindCBx;
    private javax.swing.JPanel CRelFindPanel;
    javax.swing.JComboBox LRelFindCBx;
    private javax.swing.JPanel LRelFindPanel;
    javax.swing.JComboBox SearchStrategyCBx;
    private javax.swing.JPanel SearchStrategyPanel;
    
    public javax.swing.JPanel ClassesPanel;
    private javax.swing.JLabel ConceptLabel;
    javax.swing.JTextField ConceptTField;
    javax.swing.JTextArea DescriptionField;
    private javax.swing.JLabel DescriptionLabel;
    private javax.swing.JScrollPane DescriptionScrollPane;
    private javax.swing.JPanel LNodePanel;
    private javax.swing.JPanel LinguisticKBPanel;
    public javax.swing.JSplitPane MainSplitter;
    //javax.swing.JCheckBox MatchCaseCBx;
    public javax.swing.JTable ResultsTable;
    private javax.swing.JPanel SearchContainer;
    private javax.swing.JPanel SearchResults;
    private javax.swing.JPanel StringSearchPanel;
    javax.swing.JTextField StringSearchTextField;
    private  JScrollPane SearchFilterScrollPane;
    private  JPanel SearchFilterPanel;
    private javax.swing.JLabel SynonymsLabel;
    javax.swing.JList SynonymsList;
    private javax.swing.JScrollPane SynonymsScrollPanel;
    //javax.swing.JCheckBox WholeWordCBx;
    private javax.swing.JScrollPane jScrollPane1;
	
    HashMap <String, Boolean> filterBoxMapState;
	
    /** Creates new form Linguistic_GUI */
    public Linguistic_GUI() {
        initComponents();
    }
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    private void initComponents() {//GEN-BEGIN:initComponents
        java.awt.GridBagConstraints gridBagConstraints;

        MainSplitter = new javax.swing.JSplitPane();
        LinguisticKBPanel = new javax.swing.JPanel();
        SearchContainer = new javax.swing.JPanel();
        StringSearchPanel = new javax.swing.JPanel();
        SearchFilterPanel = new JPanel();
        SearchFilterScrollPane = new JScrollPane(SearchFilterPanel);
        StringSearchTextField = new javax.swing.JTextField();
        filterBoxMapState = new HashMap<String, Boolean>();
        LNodePanel = new javax.swing.JPanel();
        ConceptLabel = new javax.swing.JLabel();
        DescriptionLabel = new javax.swing.JLabel();
        SynonymsLabel = new javax.swing.JLabel();
        ConceptTField = new javax.swing.JTextField();
        DescriptionScrollPane = new javax.swing.JScrollPane();
        DescriptionField = new javax.swing.JTextArea();
        SynonymsScrollPanel = new javax.swing.JScrollPane();
        SynonymsList = new javax.swing.JList();
        CRelFindPanel = new javax.swing.JPanel();
        CRelFindCBx = new javax.swing.JComboBox();
        LRelFindPanel = new javax.swing.JPanel();
        LRelFindCBx = new javax.swing.JComboBox();
        SearchStrategyPanel = new javax.swing.JPanel();
        SearchStrategyCBx = new javax.swing.JComboBox();
        SearchResults = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();

        ResultsTable = new javax.swing.JTable();
        ClassesPanel = new javax.swing.JPanel();

        setLayout(new javax.swing.BoxLayout(this, javax.swing.BoxLayout.X_AXIS));

        setPreferredSize(new java.awt.Dimension(500, 500));
        MainSplitter.setDividerLocation(600);
        MainSplitter.setToolTipText("Linguistic Browser Panel");


        
        LinguisticKBPanel.setBorder(new javax.swing.border.TitledBorder(null, "Linguistic KB Explorer", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("MS Sans Serif", 1, 12)));
        LinguisticKBPanel.setMinimumSize(new java.awt.Dimension(198, 371));
        LinguisticKBPanel.setPreferredSize(new java.awt.Dimension(200, 300));
        LinguisticKBPanel.setEnabled(false);
        SearchContainer.setLayout(new java.awt.GridBagLayout());

        SearchContainer.setMinimumSize(new java.awt.Dimension(352, 300));
        SearchContainer.setPreferredSize(new java.awt.Dimension(530, 300));
        StringSearchPanel.setLayout(new java.awt.GridBagLayout());

        StringSearchPanel.setBorder(new javax.swing.border.TitledBorder(new javax.swing.border.TitledBorder(""), "String Search", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("MS Sans Serif", 1, 12)));
        StringSearchPanel.setMinimumSize(new java.awt.Dimension(300, 110));
        SearchFilterPanel.setMinimumSize(new Dimension(300, 100));
        SearchFilterPanel.setLayout(new java.awt.GridBagLayout());
        SearchFilterScrollPane.setMinimumSize(new Dimension(280, 70));
        StringSearchTextField.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        StringSearchTextField.setToolTipText("Insert here search string");
        //StringSearchTextField.setMaximumSize(new java.awt.Dimension(300, 25));
        StringSearchTextField.setMinimumSize(new java.awt.Dimension(200, 25));
        StringSearchTextField.setPreferredSize(new java.awt.Dimension(300, 25));

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        StringSearchPanel.add(StringSearchTextField, gridBagConstraints);
        StringSearchPanel.add(SearchFilterScrollPane, gridBagConstraints);

//        MatchCaseCBx.setText("MatchCase");
//        MatchCaseCBx.setToolTipText("\"Check this Box to abilitate case match during word search\"");
//        MatchCaseCBx.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
//
//
//        gridBagConstraints = new java.awt.GridBagConstraints();
//        gridBagConstraints.gridx = 0;
//        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
//        gridBagConstraints.insets = new java.awt.Insets(10, 0, 0, 0);
//        StringSearchPanel.add(MatchCaseCBx, gridBagConstraints);
//
//        WholeWordCBx.setText("Whole Word Search");
//        WholeWordCBx.setToolTipText("\"Check this Box to search only for whole occurrences of the word\"");
//        WholeWordCBx.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
//
//        gridBagConstraints = new java.awt.GridBagConstraints();
//        gridBagConstraints.gridx = 0;
//        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
//        StringSearchPanel.add(WholeWordCBx, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.ipadx = 10;
        gridBagConstraints.ipady = 20;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 1.0;
        SearchContainer.add(StringSearchPanel, gridBagConstraints);

        LNodePanel.setLayout(new java.awt.GridBagLayout());

        LNodePanel.setMinimumSize(new java.awt.Dimension(200, 200));
        LNodePanel.setPreferredSize(new java.awt.Dimension(300, 200));
        ConceptLabel.setFont(new java.awt.Font("MS Sans Serif", 1, 12));
        ConceptLabel.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        ConceptLabel.setText("Sense ID");
        ConceptLabel.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        ConceptLabel.setPreferredSize(new java.awt.Dimension(80, 25));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(10, 0, 0, 0);
        LNodePanel.add(ConceptLabel, gridBagConstraints);

        DescriptionLabel.setFont(new java.awt.Font("MS Sans Serif", 1, 12));
        DescriptionLabel.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        DescriptionLabel.setText("Description");
        DescriptionLabel.setPreferredSize(new java.awt.Dimension(80, 25));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        LNodePanel.add(DescriptionLabel, gridBagConstraints);

        SynonymsLabel.setFont(new java.awt.Font("MS Sans Serif", 1, 12));
        SynonymsLabel.setText("Synonyms");
        SynonymsLabel.setPreferredSize(new java.awt.Dimension(82, 19));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weighty = 3.0;
        LNodePanel.add(SynonymsLabel, gridBagConstraints);

        ConceptTField.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        ConceptTField.setToolTipText("String representing a conceptual agglomerate of terms in the linguistic resource");
        ConceptTField.setMaximumSize(new java.awt.Dimension(200, 30));
        ConceptTField.setMinimumSize(new java.awt.Dimension(300, 25));
        ConceptTField.setPreferredSize(new java.awt.Dimension(300, 25));
        
        
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 3.0;
        gridBagConstraints.insets = new java.awt.Insets(10, 20, 20, 20);
        LNodePanel.add(ConceptTField, gridBagConstraints);

        DescriptionScrollPane.setMinimumSize(new java.awt.Dimension(300, 65));
        DescriptionScrollPane.setPreferredSize(new java.awt.Dimension(300, 200));
        DescriptionScrollPane.setAutoscrolls(true);
        DescriptionField.setFont(ConceptTField.getFont());
        DescriptionField.setLineWrap(true);
        DescriptionField.setToolTipText("A description in natural language of the choosen sense");
        DescriptionField.setWrapStyleWord(true);
        DescriptionField.setBorder(null);
        DescriptionField.setMinimumSize(new java.awt.Dimension(300, 10));
        DescriptionScrollPane.setViewportView(DescriptionField);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 3.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 20, 20, 20);
        LNodePanel.add(DescriptionScrollPane, gridBagConstraints);

        SynonymsScrollPanel.setAutoscrolls(true);
        SynonymsList.setModel(new DefaultListModel());
        SynonymsList.setToolTipText("the set of synonyms that can describe the selected sense: double click to explore the resource on the selected term");
        SynonymsList.setPreferredSize(null);


        SynonymsScrollPanel.setViewportView(SynonymsList);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.weightx = 3.0;
        gridBagConstraints.weighty = 8.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 20, 0, 20);
        LNodePanel.add(SynonymsScrollPanel, gridBagConstraints);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.gridheight = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.ipadx = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        gridBagConstraints.weightx = 10.0;
        gridBagConstraints.insets = new java.awt.Insets(0, 20, 0, 0);
        SearchContainer.add(LNodePanel, gridBagConstraints);

        CRelFindPanel.setLayout(new java.awt.GridLayout(1, 0, 20, 20));
        CRelFindPanel.setBorder(new javax.swing.border.TitledBorder(null, "explore conceptual relation", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("MS Sans Serif", 1, 12)));
        CRelFindCBx.setToolTipText("A list of semantic relations provided by the loaded linguistic resource (uses the Sense ID field)");
        CRelFindCBx.setPreferredSize(new java.awt.Dimension(24, 25));
        CRelFindPanel.add(CRelFindCBx);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        SearchContainer.add(CRelFindPanel, gridBagConstraints);

        LRelFindPanel.setLayout(new java.awt.GridLayout(1, 0));
        LRelFindPanel.setBorder(new javax.swing.border.TitledBorder(null, "explore lexical relation", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("MS Sans Serif", 1, 12)));
        LRelFindCBx.setToolTipText("a list of lexical relations for exploring the resources (needs to select one sense and one of its words)");
        LRelFindCBx.setPreferredSize(new java.awt.Dimension(24, 25));        
        LRelFindPanel.add(LRelFindCBx);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        SearchContainer.add(LRelFindPanel, gridBagConstraints);
        
        SearchStrategyPanel.setLayout(new java.awt.GridLayout(1, 0));
        SearchStrategyPanel.setBorder(new javax.swing.border.TitledBorder(null, "explore search strategies", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("MS Sans Serif", 1, 12)));
        SearchStrategyCBx.setToolTipText("a list of search strategies for exploring the resources (uses the String Search field)");
        SearchStrategyCBx.setPreferredSize(new java.awt.Dimension(24, 25));
        SearchStrategyPanel.add(SearchStrategyCBx);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        SearchContainer.add(SearchStrategyPanel, gridBagConstraints);



        SearchResults.setLayout(new javax.swing.BoxLayout(SearchResults, javax.swing.BoxLayout.Y_AXIS));

        SearchResults.setBorder(new javax.swing.border.EmptyBorder(new java.awt.Insets(10, 10, 10, 10)));
        //SearchResults.setMinimumSize(new java.awt.Dimension(43, 500));
        //SearchResults.setPreferredSize(new java.awt.Dimension(350, 600));
        //jScrollPane1.setMinimumSize(new java.awt.Dimension(23, 200));
        //jScrollPane1.setPreferredSize(new java.awt.Dimension(350, 200));
        ResultsTable.setFont(new java.awt.Font("MS Sans Serif", 0, 10));        
        ResultsTable.setModel(new ResultsTableModel());
        ResultsTable.setToolTipText("Results Table: left click or press SPACE to select a result");
        //ResultsTable.setMaximumSize(new java.awt.Dimension(2147483647, 500));
        //look at post-init code on ResultsTable Code PropertyList
        ResultsTable.getColumnModel().getColumn(0).setPreferredWidth(100);
        ResultsTable.getColumnModel().getColumn(1).setPreferredWidth(100);
        ResultsTable.getColumnModel().getColumn(2).setPreferredWidth(800);

        
        jScrollPane1.setViewportView(ResultsTable);

        SearchResults.add(jScrollPane1);
        
        LinguisticKBPanel.setLayout(new javax.swing.BoxLayout(LinguisticKBPanel, javax.swing.BoxLayout.Y_AXIS));
        LinguisticKBPanel.add(SearchContainer, BorderLayout.LINE_START);
        LinguisticKBPanel.add(SearchResults, BorderLayout.CENTER);

        MainSplitter.setLeftComponent(LinguisticKBPanel);

        ClassesPanel.setMinimumSize(new java.awt.Dimension(30, 30));
        ClassesPanel.setPreferredSize(new java.awt.Dimension(20, 20));
        MainSplitter.setRightComponent(ClassesPanel);

        add(MainSplitter);

    }
    
    public void addFilterBox(SearchFilter searchFilter){
    	JCheckBox boxCheck = new JCheckBox();
    	boxCheck.setText(searchFilter.getName());
    	boxCheck.setSelected(searchFilter.isActive());
    	boxCheck.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
    	
    	GridBagConstraints gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.WEST;
        //gridBagConstraints.insets = new java.awt.Insets(10, 0, 0, 0);
        //StringSearchPanel.add(boxCheck, gridBagConstraints);
        SearchFilterPanel.add(boxCheck, gridBagConstraints);
        
    	filterBoxMapState.put(searchFilter.getName(), searchFilter.isActive());
    	boxCheck.addItemListener(new ItemListener() {
    		public void itemStateChanged(ItemEvent ie) {
    			String idCheckBox = ((JCheckBox) ie.getItem()).getText();
    			int state = ie.getStateChange();
    			Boolean selected;
    			if (state == ItemEvent.SELECTED)
    				selected = true;
    			else
    				selected = false;
    			//filterBoxMapState.remove(idCheckBox);
    			filterBoxMapState.put(((JCheckBox) ie.getItem()).getText(), selected);
			}
           
        });
    	
    }
    

    
    
    
    // End of variables declaration//GEN-END:variables
    
}
