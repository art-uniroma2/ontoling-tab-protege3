package it.uniroma2.art.ontoling.protege.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

/**
 * @author  Armando Stellato <stellato@info.uniroma2.it, Andrea Turbati <turbati@info.uniroma2.it>
 */

public class SelectionFrame extends javax.swing.JFrame{
	private JButton buttonOK = null;
	private ButtonGroup bg = null;
	private Box box;
	
	public SelectionFrame(){
		
		JPanel panel = new JPanel();		
		BoxLayout boxLayout = new BoxLayout(panel, BoxLayout.Y_AXIS);
		panel.setLayout(boxLayout);
		
		box = Box.createVerticalBox();
		JPanel bottomPanel = new JPanel();
		bottomPanel.setBorder(new CompoundBorder(BorderFactory.createLineBorder(Color.black, 1),
		BorderFactory.createBevelBorder(BevelBorder.RAISED)));
		Border edge = BorderFactory.createRaisedBevelBorder();
		
		JPanel topPanel = new JPanel();
		topPanel.setLayout(new BorderLayout());
		topPanel.setBorder(new TitledBorder(new EtchedBorder(), "Choose a resource to use"));
		topPanel.add(box, BorderLayout.CENTER);							
		JScrollPane commonPane = new JScrollPane(topPanel);	
		
		panel.add(commonPane);
		
		
		
		Dimension size = new Dimension(130, 20);
		buttonOK = new JButton("OK");
		buttonOK.setBorder(edge);		
		buttonOK.setPreferredSize(size);
		
		bottomPanel.add(buttonOK);
		//bottomPanel.add(buttonClose);
		
		panel.add(bottomPanel);
		
		add(panel);
		
		pack();
		setSize(400, 200);
	}

	
	public String getSelection() {
		//Vector <String>vect = new Vector<String>();
		String selectId; 
		for (Enumeration e=bg.getElements(); e.hasMoreElements(); ) {
            JRadioButton b = (JRadioButton)e.nextElement();            
            if (b.getModel() == bg.getSelection()) {            	            	
            	//Selection selection = new Selection(b.getText());            	
                selectId = b.getText();
            	//vect.add(b.getText());
            	return selectId;
            }
        }		
		
		return null;
		
	}
	
	public void drawSlectionPanel(Collection<String> loadedRes){
		bg = new ButtonGroup();
		JRadioButton rb1;
		boolean first = true;
		
		if(loadedRes.size() == 0){
			//TODO gestire il caso in cui non ci siano risorse da selezionare
		}
		
		for(String idRes : loadedRes){
			rb1 = new JRadioButton(idRes, first);
			if(first)
				first = false;
			bg.add(rb1);
			box.add(rb1);
		}
	}
	
	public JButton getOKButton() {
		return buttonOK;
	}
}
