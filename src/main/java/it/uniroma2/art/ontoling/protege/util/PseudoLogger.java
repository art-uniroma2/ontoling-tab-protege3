package it.uniroma2.art.ontoling.protege.util;

import java.io.BufferedWriter;
import java.io.FileWriter;

public class PseudoLogger {

	static PseudoLogger pseudoLogger = null;
	private String name = "pseudoLogger.txt";
	
	private PseudoLogger(){
	}
	
	public static PseudoLogger getPseudoLogger(){
		if(pseudoLogger == null){
			pseudoLogger = new PseudoLogger();
		}
		return pseudoLogger;
	}
	
	public void write(String text){
		try{
			FileWriter fstream = new FileWriter(name, true);
			BufferedWriter out = new BufferedWriter(fstream);
			out.write("\n"+text);
			out.close();
			fstream.close();
		} catch (Exception e){
			  System.err.println("Error: " + e.getMessage());
		}
	}
}
