 /*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is OntoLing.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2004.
 * All Rights Reserved.
 *
 * OntoLing was developed by the Artificial Intelligence Research Group
 * (ai-nlp.info.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about OntoLing can be obtained at 
 * http//ai-nlp.info.uniroma2.it/software/OntoLing.html
 *
 */

 /*
  * Contributor(s): Armando Stellato stellato@info.uniroma2.it
 */

package it.uniroma2.art.ontoling.protege.ui;

import it.uniroma2.art.ontoling.protege.util.Util;

import java.awt.*;
import java.awt.event.*;
import java.util.*;

import javax.swing.*;

import edu.stanford.smi.protege.model.*;
import edu.stanford.smi.protege.util.*;
import edu.stanford.smi.protege.ui.*;

/**
 * 
 * @author Ray Fergerson <fergerson@smi.stanford.edu>
 * @author Armando Stellato <stellato@info.uniroma2.it>
 */
public class SetTerminologySlotPanel extends JComponent {
    private LabeledComponent _labeledComponent;
    private JComboBox _terminologySlotComboBox;
    private Cls _cls;
    private BrowserSlotPattern _currentPattern;
    private Action _refreshAction;  
    private JButton _refreshTerminologySlotButton;
    private JLabel _terminologyLabel;
    
    public SetTerminologySlotPanel() {
        createComponents();
        layoutComponents();
    }

    private void createComponents() {
        _refreshAction = new AbstractAction("refresh", null) {
            public void actionPerformed(ActionEvent e) {
                populateTerminologySlotComboBox();
            }
        };
        _terminologyLabel = ComponentFactory.createLabel("Terminology Slot: ");
        _terminologySlotComboBox = ComponentFactory.createComboBox();
        _refreshTerminologySlotButton = ComponentFactory.createButton(_refreshAction);
    }
    
    private void layoutComponents() {
        setLayout(new BorderLayout());
        add(_terminologyLabel, BorderLayout.WEST);
        add(_terminologySlotComboBox, BorderLayout.CENTER);
        add(_refreshTerminologySlotButton, BorderLayout.EAST);
        populateTerminologySlotComboBox();
    }


    
    private void populateTerminologySlotComboBox() {
        
        
        Collection values = new ArrayList();
        Collection slots = ProjectManager.getProjectManager().getCurrentProject().getKnowledgeBase().getSlots();
        Iterator i = slots.iterator();
        BrowserSlotPattern pattern;
        BrowserSlotPattern labelPattern = null;
        while (i.hasNext()) {
            Slot slot = (Slot) i.next();
            if ( (slot.getValueType().compareTo(ValueType.STRING)==0) && !(slot.getName().startsWith(":")) )
            {
                pattern = new BrowserSlotPattern(slot);
                values.add(pattern);
                if (slot.getName().equals("rdfs:label") )
                    labelPattern = pattern; 
            }
        }
        _terminologySlotComboBox.setRenderer(new TerminologySlotRenderer());
        DefaultComboBoxModel model = new DefaultComboBoxModel(values.toArray());
        _terminologySlotComboBox.setModel(model);
        if ( Util.kbInOWL(ProjectManager.getProjectManager().getCurrentProject().getKnowledgeBase()) )
            _terminologySlotComboBox.setSelectedItem(labelPattern);
 //     setEnabled(true);
    }
    
   
    public void setEnabled(boolean b) {
        _labeledComponent.setComponentsEnabled(b);
    }
    
    public Slot getSelectedSlot()
    { 
        BrowserSlotPattern b_slot_patt = (BrowserSlotPattern)_terminologySlotComboBox.getSelectedItem();
        if (b_slot_patt != null)
        	return b_slot_patt.getFirstSlot();
		else return null;
    }
}
