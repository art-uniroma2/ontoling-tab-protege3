 /*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is OntoLing.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2004.
 * All Rights Reserved.
 *
 * OntoLing was developed by the Artificial Intelligence Research Group
 * (ai-nlp.info.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about OntoLing can be obtained at 
 * http//ai-nlp.info.uniroma2.it/software/OntoLing.html
 *
 */

 /*
  * Contributor(s): Armando Stellato stellato@info.uniroma2.it
 */

package it.uniroma2.art.ontoling.protege;

import it.uniroma2.art.lw.manager.LinguisticWatermarkManager;
import it.uniroma2.art.lw.model.objects.LinguisticInterface;
import it.uniroma2.art.ontoling.OntoLingCore;
import it.uniroma2.art.ontoling.exceptions.OntoLingInitializationException;
import it.uniroma2.art.ontoling.protege.ui.GUI_Manager;
import it.uniroma2.art.ontoling.protege.ui.OntolingMenu;
import it.uniroma2.art.ontoling.protege.ui.GUI_Facade;
import it.uniroma2.art.ontoling.protege.util.Util;

import java.awt.Frame;
import java.io.File;
import java.util.ArrayList;

import javax.swing.JMenuBar;
import javax.swing.JOptionPane;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import edu.stanford.smi.protege.model.Project;
import edu.stanford.smi.protege.ui.ProjectManager;
import edu.stanford.smi.protege.widget.AbstractTabWidget;

// The Linguistic Tab
public class OntoLingTab extends AbstractTabWidget {
    /**
     * 
     */
    private static final long serialVersionUID = 3113346209767634397L;
    protected static Log logger = LogFactory.getLog(OntoLingTab.class);
    private static OntoLingTab ontoLingTab;
    OntolingMenu _menu =  new OntolingMenu();
    OntoLingCore ontLingCore;
    GUI_Facade guiFacade;
    
        // startup code
    public void initialize() {    	     	    	    	    	
        logger.info("\n***************\n***************\n\nONTOLING INITIALIZATION\n\n***************\n***************\n***************");
        ontoLingTab = this;
        guiFacade = GUI_Facade.getInstance();
        removeAll();
    	revalidate();
    	// initialize the tab text        
    	setLabel("OntoLing");
    	    	
    	File oSGiDir = new File("plugins/it.uniroma2.art.ontoling/Felix");
    	//ArrayList <File>dirList = new ArrayList<File>();
        //dirList.add(new File("plugins/it.uniroma2.art.ontoling/bundle"));
        String rootDirBundle = "plugins/it.uniroma2.art.ontoling";
    	File configFile = new File("plugins/it.uniroma2.art.ontoling/properties/LWConfig.xml");        
        try {
            ontLingCore = OntoLingCore.initialize(oSGiDir, rootDirBundle, false, configFile);
        } catch (OntoLingInitializationException e) {
            String errorMsg = "some problems occurred during OntoLing initialization\n" +
            "Initialization Exception Message:\n" + e.getMessage();
            Frame frame = JOptionPane.getRootFrame();
            JOptionPane.showMessageDialog(frame, errorMsg, "OntoLing Initialization Error!", JOptionPane.ERROR_MESSAGE);
        }
    	
        _menu.selectionAction();
        validate(getProject());
        JMenuBar menuBar = getMainWindowMenuBar();
        menuBar.add (_menu);
    }
    
    public void reinitialize(){
    	logger.info("\n***************\n***************\n\nONTOLING RE_INITIALIZATION\n\n***************\n***************\n***************");
        removeAll();
    	revalidate();
    	// initialize the tab text        
    	setLabel("OntoLing");
    	validate(getProject());
    	JMenuBar menuBar = getMainWindowMenuBar();
        menuBar.add (_menu);
    }
    
    
    public void validate(Project project){
        GUI_Manager.create_GUI(project);
        add(GUI_Manager.getLinguisticGUI());
        System.out.println( "User directory:" + System.getProperty("user.dir") );
        try {initialize_LinguisticResource(); } catch (Exception ex) {ex.printStackTrace(); };// da cancellare printstacktrac
        
        /*
         * LING_ENRICH  
        if ( ontLingCore.isLRSuitableForAutomaticEnrichment(LinguisticWatermarkManager.getSelectedInterface() ) && Util.kbInOWL(ProjectManager.getProjectManager().getCurrentProject().getKnowledgeBase())) {
            GUI_Manager.getOntologyPanel().addEnrichmentGUI();
        }
        */
    } 
    
    public GUI_Facade getGUIFacade() {
    	return guiFacade;
    }
    
    private void initialize_LinguisticResource() throws SecurityException, ClassNotFoundException, NoSuchMethodException {
    	LinguisticInterface LInst = LinguisticWatermarkManager.getSelectedInterface();
    	if(LInst != null) {
	        guiFacade.initialize_GUI(LInst);                
	        guiFacade.initializeMethods_on_KBChange(ProjectManager.getProjectManager().getCurrentProject().getKnowledgeBase());
	        guiFacade.initializeListeners();
	    }
    }
    
    
    public static OntoLingTab getOntoLingTab() {
    	return ontoLingTab;
    }
    
    // this method is useful for debugging
    public static void main(String[] args) {
//      String[] projarg = {"C:\\Development\\Protege_2.0\\examples\\testLinguisticR\\testLinguisticR.pprj"};
    	String[] projarg = {};	
    	System.out.println( "User directory:" + System.getProperty("user.dir") );
        System.setProperty("user.dir","F:/Development/Protege_3.2.1");
        System.out.println( "User directory:" + System.getProperty("user.dir") );
        edu.stanford.smi.protege.Application.main( projarg );
    }         
}

