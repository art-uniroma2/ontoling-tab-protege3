 /*
 * The contents of this file are subject to the Mozilla Public License
 * Version 1.1 (the "License");  you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 * http//www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
 * the specific language governing rights and limitations under the License.
 *
 * The Original Code is OntoLing.
 *
 * The Initial Developer of the Original Code is University of Roma Tor Vergata.
 * Portions created by University of Roma Tor Vergata are Copyright (C) 2004.
 * All Rights Reserved.
 *
 * OntoLing was developed by the Artificial Intelligence Research Group
 * (ai-nlp.info.uniroma2.it) at the University of Roma Tor Vergata
 * Current information about OntoLing can be obtained at 
 * http//ai-nlp.info.uniroma2.it/software/OntoLing.html
 *
 */

 /*
  * Contributor(s): Armando Stellato stellato@info.uniroma2.it
 */

/* 
 * TabbedPaneDemo.java is a 1.4 example that requires one additional file:
 *   images/middle.gif. 
 */
package it.uniroma2.art.ontoling.protege.ui;

import java.awt.BorderLayout;
import java.awt.event.KeyEvent;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import edu.stanford.smi.protege.model.Project;
import edu.stanford.smi.protege.model.Slot;
import edu.stanford.smi.protege.util.ComponentFactory;

public class OntologyPanel extends JPanel {

    /**
     * 
     */
    private static final long serialVersionUID = 2093009269921798671L;

        //        ImageIcon icon = createImageIcon("images/middle.gif");
        SetTerminologySlotPanel _setTerminologySlotPanel;

		private ClsesPanel linguisticClsesPanel;
		private SubslotPane linguisticSlotPane;
        private JTabbedPane tabpanel;
    	
        private JPanel LingEnrichPanel;

    
    public OntologyPanel(Project project)
    {   
        tabpanel = ComponentFactory.createTabbedPane(true);
        
        _setTerminologySlotPanel = new SetTerminologySlotPanel();

        linguisticClsesPanel = new ClsesPanel(project, _setTerminologySlotPanel);
        linguisticSlotPane = new SubslotPane(project, _setTerminologySlotPanel);
        
        tabpanel.addTab("Classes", linguisticClsesPanel);
        tabpanel.setMnemonicAt(0, KeyEvent.VK_1);
        tabpanel.addTab("Slots", linguisticSlotPane);
        tabpanel.setMnemonicAt(1, KeyEvent.VK_2);
        
        setLayout(new BorderLayout(10,10));

        add(_setTerminologySlotPanel, BorderLayout.NORTH);
        add(tabpanel, BorderLayout.CENTER); 
    }  
    
    public void swapTab(int index){
    	tabpanel.setSelectedIndex(index);
    }
    
    public ClsesPanel getInstanceOfClsesPanel(){
    	return linguisticClsesPanel;
    }
    
    public SubslotPane getInstanceOfSubslotPane(){
    	return linguisticSlotPane;
    }
    
    public Slot getTerminologySlot()
    {
        return _setTerminologySlotPanel.getSelectedSlot();
    }
    
    /*
     * LING_ENRICH
     * 
    public void addEnrichmentGUI(){
        LingEnrichPanel = new EnrichmentPanel();
        add(LingEnrichPanel,BorderLayout.PAGE_END);
    }
    */
    
    public void removeEnrichmentGUI(){
    	remove(LingEnrichPanel);
    }
    
    //Uncomment the following line to use scrolling tabs.
        //tabbedPane.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT)
}
