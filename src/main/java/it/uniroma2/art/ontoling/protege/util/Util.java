 /*
  * The contents of this file are subject to the Mozilla Public License
  * Version 1.1 (the "License");  you may not use this file except in compliance
  * with the License.  You may obtain a copy of the License at
  * http//www.mozilla.org/MPL/
  *
  * Software distributed under the License is distributed on an "AS IS" basis,
  * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
  * the specific language governing rights and limitations under the License.
  *
  * The Original Code is OntoMap.
  *
  * The Initial Developer of the Original Code is University of Roma Tor Vergata.
  * Portions created by University of Roma Tor Vergata are Copyright (C) 2004.
  * All Rights Reserved.
  *
  * OntoMap was developed by the Artificial Intelligence Research Group
  * (ai-nlp.info.uniroma2.it) at the University of Roma Tor Vergata
  * Current information about OntoMap can be obtained at 
  * http//ai-nlp.info.uniroma2.it/software/ontomap
  *
  * Contributor(s): Armando Stellato stellato@info.uniroma2.it
  */
package it.uniroma2.art.ontoling.protege.util;


import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;

import edu.stanford.smi.protege.model.Cls;
import edu.stanford.smi.protege.model.KnowledgeBase;
import edu.stanford.smi.protege.model.Slot;
import edu.stanford.smi.protege.ui.ProjectManager;;


/**
 * @author Armando Stellato
 *
 */
public class Util {
	
	public static KnowledgeBase getCurrentKB(){
		return ProjectManager.getProjectManager().getCurrentProject().getKnowledgeBase();
	}

	public static void displayErrors(Collection errors) {
		Iterator i = errors.iterator();
		while (i.hasNext()) {
			System.out.println("Error: " + i.next());
		}
	}  
	  
	public static boolean kbInOWL(KnowledgeBase kb) {
		int index = (kb.getClass().getName().indexOf("OWL"));
		return (index > 0);
	}	  
	
	
	public static Cls createClassThroughLabels(KnowledgeBase kb, String[] labels, Collection parents) {  
	    boolean nameFound = false;
	    String tempName=null;
	    for (int i = 0; i<labels.length && nameFound==false; i++) 
	    	if (kb.getFrame(labels[i]) == null) {
	    	    tempName = labels[i];
	    	    nameFound = true;
	    	}	
	    for (int i = 0; i<labels.length && nameFound==false; i++)
	        for (int j = i+1; j<labels.length && nameFound==false; j++)
		    	if (kb.getFrame(labels[i] + "-" + labels[j]) == null)
		    	{
		    	    tempName = labels[i] + "-" + labels[j];
		    	    nameFound = true;
		    	}
		for (int i = 0; nameFound==false; i++)
		    if (kb.getFrame(labels[0] + "_" + i) == null)
		    {
		        tempName=labels[0] + "_" + i;
		        nameFound = true;
		    }    
		
		//to create a class now we first create a class with null as name and then we change its name to the desired one
		Cls newClass = kb.createCls(null, parents);
        String className = newClass.getName();
        int lastPos = className.lastIndexOf("Class_");
        String newClassName = className.substring(0, lastPos)+tempName;
        newClass.rename(newClassName);
        return newClass;
	    //return kb.createCls(tempName, parents);
	}
	
	public static Slot createSlotThroughLabels(KnowledgeBase kb, String[] labels, Collection parents) {  
		boolean nameFound = false;
	    String tempName=null;
	    for (int i = 0; i<labels.length && nameFound==false; i++) 
	    	if (kb.getFrame(labels[i]) == null)
	    	{
	    	    tempName = labels[i];
	    	    nameFound = true;
	    	}	
	    for (int i = 0; i<labels.length && nameFound==false; i++)
	        for (int j = i+1; j<labels.length && nameFound==false; j++)
		    	if (kb.getFrame(labels[i] + "-" + labels[j]) == null)
		    	{
		    	    tempName = labels[i] + "-" + labels[j];
		    	    nameFound = true;
		    	}
		for (int i = 0; nameFound==false; i++)
		    if (kb.getFrame(labels[0] + "_" + i) == null)
		    {
		        tempName=labels[0] + "_" + i;
		        nameFound = true;
		    }
		//to create a class now we first create a class with null as name and then we change its name to the desired one
		Slot newSlot = kb.createSlot(null, null, parents, true);
        String slotName = newSlot.getName();
        int lastPos = slotName.lastIndexOf("testDatarange_Class");
        String newSlotName = slotName.substring(0, lastPos)+tempName;
        newSlot.rename(newSlotName);
        return newSlot;
	    //return kb.createSlot(tempName, null, parents, true);
	}	
    
    public static void pause() {
        System.out.println("pause...type one number");
        try {
        	System.in.read();
        } 
        catch (IOException e) {
        	e.printStackTrace();
        }
    }
    
}
