 /*
  * The contents of this file are subject to the Mozilla Public License
  * Version 1.1 (the "License");  you may not use this file except in compliance
  * with the License.  You may obtain a copy of the License at
  * http//www.mozilla.org/MPL/
  *
  * Software distributed under the License is distributed on an "AS IS" basis,
  * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
  * the specific language governing rights and limitations under the License.
  *
  * The Original Code is OntoLing.
  *
  * The Initial Developer of the Original Code is University of Roma Tor Vergata.
  * Portions created by University of Roma Tor Vergata are Copyright (C) 2004.
  * All Rights Reserved.
  *
  * OntoLing was developed by the Artificial Intelligence Research Group
  * (ai-nlp.info.uniroma2.it) at the University of Roma Tor Vergata
  * Current information about OntoLing can be obtained at 
  * http//ai-nlp.info.uniroma2.it/software/OntoLing.html
  *
  */

  /*
   * Contributor(s): Armando Stellato stellato@info.uniroma2.it
  */

 package it.uniroma2.art.ontoling.protege.ui;


 import java.util.Vector;

import javax.swing.table.AbstractTableModel;



 /**
 * @author Emanuele Rodol� 
 * @author Armando Stellato <stellato@info.uniroma2.it>
 */
public class ResultsTableModel_LE extends AbstractTableModel {
    
     /** vector to store the data in the columns */
    protected Vector data;
     /** column names - can be changed in subclasses */
    protected String[] cols = new String[] { "Sense","Gloss","Plausibility"};

     /** constructor */
    public ResultsTableModel_LE() {
       super();
       data = new Vector();
    }
    
     /**
       * Returns the type of object in the column
       * @param c the column
       * @return the type of class
       */
    public Class getColumnClass(int c) {
       return getValueAt(0,c).getClass();
    }
    
     /**
       * Returns the total number of rows
       * @return the # of rows
       */
    public int getRowCount() { return data.size();}
    
     /**
       * Clears all rows in the table
       */
    public void clear() {
       data.removeAllElements();
       fireTableDataChanged();
    }
    
     /**
       * Removes a row in the table
       * @param i the row number
       */
    public void removeRow( int i ) {
       data.removeElementAt(i);
       fireTableRowsDeleted(i,i);
    }
     
     /**
       * Returns the total number of columns in the table
       * @return the # of cols
       */
    public int getColumnCount() { return cols.length; }
    
     /**
       * Returns the column name for a given column
       * @param i the column
       * @return the name of that column
       */
    public String getColumnName(int i) { return cols[i]; }

    /**
     * Returns the column name for a given column
     * @param title the title of the column
     * @param i the column
     */   
    public void setColumnName(String title, int i) { cols[i] = title;}
    
     /**
       * Returns the object in the given coordinates
       * may be overridden by methods that extend this class
       * @param row the row number
       * @param col the column number
       * @return the object in that row/col
       */
    public Object getValueAt(int row, int col) {
       Object[] o = (Object[])data.elementAt(row);
       return o[col];
    }
    
     /**
       * Add a row to the table... if the entry is null, stick an empty string
       * in it's place, otherwise the table throws NullPointers
       * @param o an arrow of objects making 1 row of the table
       */
    public void addRow(Object o[]) {
       for( int i=0; i<o.length; i++ ) {
          if( o[i] == null ) {
             o[i] = new String();
          }
       }
       data.addElement(o);
       //data.
        // entry added to the end, so fire an event to notify the table
        // to update itself
       fireTableRowsInserted(data.size()-1,data.size()-1);
    }

    
 }
