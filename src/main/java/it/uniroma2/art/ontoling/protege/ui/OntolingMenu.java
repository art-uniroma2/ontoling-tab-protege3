package it.uniroma2.art.ontoling.protege.ui;

import it.uniroma2.art.lw.config.ui.ConfigureFrame;
import it.uniroma2.art.lw.config.ui.LoaderFrame;
import it.uniroma2.art.lw.config.ui.Selection;
import it.uniroma2.art.lw.exceptions.PropertyNotFoundException;
import it.uniroma2.art.lw.manager.LinguisticWatermarkManager;
import it.uniroma2.art.ontoling.protege.OntoLingTab;

import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Collection;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JMenu;

public class OntolingMenu extends JMenu {
	protected AbstractAction _configure = new ConfigureAction();
	protected AbstractAction _loader = new LoaderAction();
	protected AbstractAction _selection = new SelectionAction();

	private boolean firstConfigure = true;
	private boolean firstLoader = true;

	// private boolean firstSelection = true;

	public OntolingMenu() {
		super("OntoLing");

		JMenu submenu = new JMenu("Configure");
		submenu.add(_configure);
		add(submenu);

		add(_loader);

		add(_selection);

		_configure.setEnabled(true);
		_loader.setEnabled(true);
		_selection.setEnabled(true);

	}

	public void selectionAction() {
		_selection.actionPerformed(null);
	}

	private class ConfigureAction extends AbstractAction {
		public ConfigureAction() {
			super("LinguisticWatermarkConfiguration");
		}

		public void actionPerformed(ActionEvent e) {
			final ConfigureFrame frame = LinguisticWatermarkManager.getConfigureFrame();
			try {
				frame.drawConfigurePanel();
			} catch (PropertyNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			frame.setVisible(true);
			if (firstConfigure) {
				firstConfigure = false;
				JButton jbuttonSave = frame.getSaveButton();
				ActionListener listeners[] = jbuttonSave.getActionListeners();
				for (int i = 0; i < listeners.length; ++i) {
					jbuttonSave.removeActionListener(listeners[i]);
				}
				jbuttonSave.addActionListener(new ActionListener() {

					public void actionPerformed(ActionEvent arg0) {
						_loader.actionPerformed(null);
					}
				});
				for (int i = 0; i < listeners.length; ++i) {
					jbuttonSave.addActionListener(listeners[i]);
				}
			}

		}

	}

	private class LoaderAction extends AbstractAction {
		public LoaderAction() {
			super("Loader");
		}

		public void actionPerformed(ActionEvent e) {
			final LoaderFrame frame = (LoaderFrame) LinguisticWatermarkManager.getLoaderFrame();
			frame.drawSelectionPanel();
			frame.setVisible(true);
			if (firstLoader) {
				firstLoader = false;
				JButton jbuttonOK = frame.getOKButton();
				ActionListener listeners[] = jbuttonOK.getActionListeners();
				for (int i = 0; i < listeners.length; ++i) {
					jbuttonOK.removeActionListener(listeners[i]);
				}
				jbuttonOK.addActionListener(new ActionListener() {

					public void actionPerformed(ActionEvent arg0) {
						Collection<String> loadedRes = new Vector<String>();

						Vector<Selection> vectLoadedResSel = frame.getSelection();
						for (int i = 0; i < vectLoadedResSel.size(); ++i) {
							Selection sel = vectLoadedResSel.get(i);
							loadedRes.add(sel.getInstanceId());
						}

						LinguisticWatermarkManager.setLoadedResName(loadedRes);

						LinguisticWatermarkManager.save();

						frame.dispose();

						_selection.actionPerformed(null);
					}
				});
				for (int i = 0; i < listeners.length; ++i) {
					jbuttonOK.addActionListener(listeners[i]);
				}
			}
		}

	}

	private class SelectionAction extends AbstractAction {

		public SelectionAction() {
			super("Selection");
		}

		public void actionPerformed(ActionEvent e) {

			final SelectionFrame frameSelection = new SelectionFrame();

			frameSelection.drawSlectionPanel(LinguisticWatermarkManager.getLoadedResName());
			frameSelection.setVisible(true);
			JButton jbuttonOK = frameSelection.getOKButton();
			jbuttonOK.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					OntoLingTab tab = OntoLingTab.getOntoLingTab();
					String instId = frameSelection.getSelection();
					LinguisticWatermarkManager.setLoadedInterface(instId);
					LinguisticWatermarkManager.save();
					tab.reinitialize();
					frameSelection.dispose();

				}
			});

		}

	}

}
