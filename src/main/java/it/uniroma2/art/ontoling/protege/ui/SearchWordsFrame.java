package it.uniroma2.art.ontoling.protege.ui;

import it.uniroma2.art.lw.model.objects.SearchFilter;
import it.uniroma2.art.lw.model.objects.SearchWord;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.util.Arrays;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Iterator;

import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

/**
 * @author  Andrea Turbati <turbati@info.uniroma2.it>
 */

public class SearchWordsFrame extends javax.swing.JFrame{
	private JButton buttonOK = null;
	private ButtonGroup bg = null;
	private Box box;
	
	public SearchWordsFrame(Collection <SearchWord> searchWords){
		
		JPanel panel = new JPanel();		
		BoxLayout boxLayout = new BoxLayout(panel, BoxLayout.Y_AXIS);
		panel.setLayout(boxLayout);
		
		box = Box.createVerticalBox();
		JPanel bottomPanel = new JPanel();
		bottomPanel.setBorder(new CompoundBorder(BorderFactory.createLineBorder(Color.black, 1),
		BorderFactory.createBevelBorder(BevelBorder.RAISED)));
		Border edge = BorderFactory.createRaisedBevelBorder();
		
		JPanel topPanel = new JPanel();
		topPanel.setLayout(new BorderLayout());
		topPanel.setBorder(new TitledBorder(new EtchedBorder(), "Choose a searchWord"));
		topPanel.add(box, BorderLayout.CENTER);							
		JScrollPane commonPane = new JScrollPane(topPanel);	
		
		panel.add(commonPane);
		
		
		Dimension size = new Dimension(130, 20);
		buttonOK = new JButton("OK");
		buttonOK.setBorder(edge);		
		buttonOK.setPreferredSize(size);
		
		bottomPanel.add(buttonOK);
		//bottomPanel.add(buttonClose);
		
		panel.add(bottomPanel);
		
		add(panel);
		
		pack();
		
		
		if(searchWords.size() < 10)
			setSize(350, (searchWords.size()*25)+80);
		else
			setSize(350, 300);
		
		drawSlectionPanel(searchWords);
	}

	
	public SearchWord getSelection() {
		String searchWordString; 
		for (Enumeration<AbstractButton>e=bg.getElements(); e.hasMoreElements(); ) {
            JRadioButton b = (JRadioButton)e.nextElement();            
            if (b.getModel() == bg.getSelection()) {            	            	
            	searchWordString = b.getText();
            	return new SearchWord(searchWordString);
            }
        }		
		
		return null;
		
	}
	
	private void drawSlectionPanel(Collection<SearchWord> searchWords){
		bg = new ButtonGroup();
		JRadioButton rb1;
		String searchWordsArray[];
		boolean first = true;
		
		if(searchWords.size() == 0){
			return;
		}
		
		//order the searchWord in alphabetical order
		searchWordsArray = new String[searchWords.size()];
		Iterator <SearchWord>iterSW = searchWords.iterator();
		for(int i=0; i<searchWords.size(); ++i){
			searchWordsArray[i] = iterSW.next().getName();
		}
		Arrays.sort(searchWordsArray);
		
		
		for(String searchWordString : searchWordsArray){
			rb1 = new JRadioButton(searchWordString, first);
			if(first)
				first = false;
			bg.add(rb1);
			box.add(rb1);
		}
	}
	
	public JButton getOKButton() {
		return buttonOK;
	}
}
