 /*
  * The contents of this file are subject to the Mozilla Public License
  * Version 1.1 (the "License");  you may not use this file except in compliance
  * with the License.  You may obtain a copy of the License at
  * http//www.mozilla.org/MPL/
  *
  * Software distributed under the License is distributed on an "AS IS" basis,
  * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
  * the specific language governing rights and limitations under the License.
  *
  * The Original Code is ONTOLING_2.0.
  *
  * The Initial Developer of the Original Code is University of Roma Tor Vergata.
  * Portions created by University of Roma Tor Vergata are Copyright (C) 2004.
  * All Rights Reserved.
  *
  * ONTOLING_2.0 was developed by the Artificial Intelligence Research Group
  * (ai-nlp.info.uniroma2.it) at the University of Roma Tor Vergata
  * Current information about ONTOLING_2.0 can be obtained at 
  * http//ai-nlp.info.uniroma2.it/software/...
  *
  */

  /*
   * Contributor(s): Armando Stellato stellato@info.uniroma2.it
  */
package it.uniroma2.art.ontoling.protege.test.core_run;

import it.uniroma2.art.lw.model.objects.SemanticIndex;
import it.uniroma2.art.ontoling.protege.OntoLingTab;
import it.uniroma2.art.ontoling.protege.enrichment.LinguisticEnricherProxy;
import it.uniroma2.art.ale.LinguisticEnrichmentInterface;
import it.uniroma2.art.ale.storage.inmemory.PlausibilityStateImplWriteAllowed;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.log4j.Logger;

import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntModelSpec;
import com.hp.hpl.jena.ontology.OntResource;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.reasoner.ReasonerRegistry;

import edu.stanford.smi.protege.model.Frame;
import edu.stanford.smi.protege.model.KnowledgeBase;
import edu.stanford.smi.protegex.owl.ProtegeOWL;
import edu.stanford.smi.protegex.owl.jena.JenaOWLModel;
import edu.stanford.smi.protegex.owl.model.OWLModel;
import edu.stanford.smi.protegex.owl.model.RDFResource;

public class AutoRun {

    private JenaOWLModel jenaOWLModel;
    private LinguisticEnricherProxy lEnr;
    private OntModel m;
    private KnowledgeBase kb;
    private LinguisticEnrichmentInterface lEnrInt; 
    final private static Logger log = Logger.getLogger(OntoLingTab.class);

    /**
     * @param enr
     */
    public AutoRun(String ontologyFilePath) {
        File file = new File(ontologyFilePath);
        URI uri=null;
        try {
            uri = new URI("file:///" + file.getAbsolutePath().replace('\\', '/'));
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        System.out.println("uri: " + uri.toString());
        
        try {
            jenaOWLModel = ProtegeOWL.createJenaOWLModelFromURI(uri.toString());
            OWLModel owlModel=(OWLModel)jenaOWLModel;
            kb=(KnowledgeBase)owlModel;
        } catch (Exception e) {     
            e.printStackTrace(); System.exit(0);
        }
        //m= jenaOWLModel.getOntModel();
        OntModelSpec spec = new OntModelSpec( OntModelSpec.OWL_DL_MEM );
        spec.setReasoner(ReasonerRegistry.getTransitiveReasoner());
        m = ModelFactory.createOntologyModel(spec, jenaOWLModel.getOntModel());

        /*
        System.out.println(System.getProperty("user.dir"));
        System.out.println("\n***************\n***************\n\nLINGUISTIC RESOURCE INITIALIZATION\n\n***************\n***************\n***************");
        LinguisticInterfacesFactory.setProperties("ONTOLING", "lresources/LWConfig.xml");
        LinguisticInterfacesFactory.loadLinguisticResources();          
        LinguisticInterface LInst = LinguisticInterfacesFactory.getLinguisticInterface();       
        System.out.print("Linguistic Resource initialization check: ");System.out.println(LInst!=null);
        */
        
    }
    
    
    private class FakeConcept extends SemanticIndex {

        public String getConceptRepresentation() {
            // TODO Auto-generated method stub
            return "mario";
        }
        
    }
   
    
    private class ResourceSemexInfo {
        private double startPlaus;
        private Collection evidenceCollection;
        private ArrayList<PlausibilityStateImplWriteAllowed> plausStates;
        private SemanticIndex semex;
        private OntResource ontoConcept;
        
        public ResourceSemexInfo(OntResource ontoConcept, SemanticIndex semex, double startPlaus, int state) {
            this.startPlaus = startPlaus; //this is a duplicate of the startPlaus which is set at step 0 two rows after...think about removing it...
            plausStates = new ArrayList();
            //this row sets plaus and state at step 0 (= index 0 of plausStates, because it is an "add" performed over the new empty ArrayList).
            plausStates.add(new PlausibilityStateImplWriteAllowed(ontoConcept, semex, startPlaus, state) );
            this.semex = semex;
            this.ontoConcept = ontoConcept;
        }   
        
        public Collection getEvidenceCollection() {
            return evidenceCollection;
        }
        
        public void setEvidenceCollection(Collection evidenceCollection) {
            this.evidenceCollection = evidenceCollection;
        }
        
        public double getStartPlaus() {
            return startPlaus;
        }   
        
        /**
         * sets the plausibility and state for the res-semex pair object at step <code>step</code>
         * @param plaus the plausibility which is to be set
         * @param state the state for the given resource-semex pair
         * @param step the step this plausibility-state pair (for the res-semex pair) is referring to
         */
        public void setPlausStateAtStep(double plaus, int state, int step) {
            //System.out.println("step: " + step + " on size: " + plausStates.size());
            plausStates.add(step, new PlausibilityStateImplWriteAllowed(ontoConcept, semex, plaus, state));
        }
        
        /**
         * gets the plausibility and state for the res-semex pair object at step <code>step</code>
         * @param step the step this plausibility-state pair (for the res-semex pair) is referring to
         */
        public PlausibilityStateImplWriteAllowed getPlausStateAtStep(int step) {
            return (PlausibilityStateImplWriteAllowed)plausStates.get(step);
        }
    
    }
    
    
    private MultiKey obtainMultiKey(OntResource res, SemanticIndex sense) {
        return new MultiKey(res.getURI(), sense);
    }
    
    private OntResource convertProtegeResource2OntResource(RDFResource rdfRes) {
        return m.getOntResource( rdfRes.getURI() );
    }

    private OntResource convertProtegeResource2OntResource(Frame rdfRes) {
        return m.getOntResource( ((RDFResource)rdfRes).getURI() );
    }
    
    
    public void test() {
        HashMap map = new HashMap();
        //MultiK
        Iterator i = kb.getClses().iterator();
        while(i.hasNext()) {
            Frame c = (Frame)i.next();
            OntResource res = convertProtegeResource2OntResource(c);
            System.out.println(res.getLocalName());
        }
        i = kb.getClses().iterator();
        //i = jenaOWLModel.listOWLNamedClasses();
        i.next();        i.next();        i.next();        i.next();
        i.next();        i.next();        i.next();        i.next();
        i.next();        i.next();        i.next();        i.next();
        RDFResource protclass = (RDFResource)i.next();
        System.out.println(protclass.getURI());
        OntResource res = m.getOntResource( protclass.getURI() );
        SemanticIndex sense = new FakeConcept();
        MultiKey res_semex = new MultiKey(res.getURI(), "mario");
        map.put(res_semex, new ResourceSemexInfo(res, sense, 34, 0));
        System.out.println(((ResourceSemexInfo)map.get(new MultiKey(res.getURI(), "mario"))).getPlausStateAtStep(0).getPlausibility());    
        ((ResourceSemexInfo)map.get(new MultiKey(res.getURI(), "mario"))).setPlausStateAtStep(35, 99, 0);
        System.out.println(((ResourceSemexInfo)map.get(new MultiKey(res.getURI(), "mario"))).getPlausStateAtStep(0).getPlausibility());
        ((ResourceSemexInfo)map.get(new MultiKey(res.getURI(), "mario"))).setPlausStateAtStep(45, 100, 1);
        System.out.println(((ResourceSemexInfo)map.get(new MultiKey(res.getURI(), "mario"))).getPlausStateAtStep(1).getPlausibility());
    }
        
    
    
    public static void main(String args[]) {
        String ontologyFilePath=args[0];
        AutoRun aut = new AutoRun(ontologyFilePath); 
        aut.test();
    }
    
}
