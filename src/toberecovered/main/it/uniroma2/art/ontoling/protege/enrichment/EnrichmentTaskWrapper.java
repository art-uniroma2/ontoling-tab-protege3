 /*
  * The contents of this file are subject to the Mozilla Public License
  * Version 1.1 (the "License");  you may not use this file except in compliance
  * with the License.  You may obtain a copy of the License at
  * http//www.mozilla.org/MPL/
  *
  * Software distributed under the License is distributed on an "AS IS" basis,
  * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
  * the specific language governing rights and limitations under the License.
  *
  * The Original Code is ONTOLING_2.0.
  *
  * The Initial Developer of the Original Code is University of Roma Tor Vergata.
  * Portions created by University of Roma Tor Vergata are Copyright (C) 2006.
  * All Rights Reserved.
  *
  * ONTOLING_2.0 was developed by the Artificial Intelligence Research Group
  * (ai-nlp.info.uniroma2.it) at the University of Roma Tor Vergata
  * Current information about ONTOLING_2.0 can be obtained at 
  * http//ai-nlp.info.uniroma2.it/software/...
  *
  */

  /*
   * Contributor(s): Armando Stellato stellato@info.uniroma2.it
  */
package it.uniroma2.art.ontoling.protege.enrichment;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;


import com.hp.hpl.jena.ontology.OntResource;

public class EnrichmentTaskWrapper {
    private int lengthOfTask;
    private int current = 0;
    private boolean done = false;
    private boolean canceled = false;
    private String statMessage;
    Jena2Protege2JenaLinguisticEnricherWrapper lEnrWrapper;
    protected static Log logger = LogFactory.getLog(EnrichmentTaskWrapper.class);

    public EnrichmentTaskWrapper(Jena2Protege2JenaLinguisticEnricherWrapper lEnrWrapper) {
        this.lEnrWrapper = lEnrWrapper;
        //Compute length of task...
        //In a real program, this would figure out
        //the number of bytes to read or whatever.
        lengthOfTask = 100; //resolved in percentage, because number of classes is not known a-priori
    }

    /**
     * Called from <CallingClass> to start the task.
     */
    public void go() {
        final SwingWorker worker = new SwingWorker() {
            public Object construct() {
                done = false;
                canceled = false;
                statMessage = null;
                return new ActualTask();
            }
        };
        worker.start();
    }

    /**
     * Called from <CallingClass> to find out how much work needs
     * to be done.
     */
    public int getLengthOfTask() {
        return lengthOfTask;
    }

    /**
     * Called from <CallingClass> to find out how much has been done.
     */
    public int getCurrent() {
        logger.debug("Number of Processed Resources: " + lEnrWrapper.getPercentageOfProcessedResources());
        return lEnrWrapper.getPercentageOfProcessedResources(); 
    }

    public void stop() {
        canceled = true;
        statMessage = null;
    }

    /**
     * Called from <CallingClass> to find out if the task has completed.
     */
    public boolean isDone() {
        return done;
    }

    /**
     * Returns the most recent status message, or null
     * if there is no current status message.
     */
    public String getMessage() {
        OntResource res = lEnrWrapper.getCurrentEnrichingResource(); 
        if (res==null) return "";
        else return res.getLocalName();
    }

    /**
     * The actual long running task.  This runs in a SwingWorker thread.
     */
    class ActualTask {
        ActualTask() {
            //Fake a long task,
            //making a random amount of progress every second.
              lEnrWrapper.init();
              try {
                Thread.sleep(300);
              } catch (InterruptedException e) {
                e.printStackTrace();
              }
              lEnrWrapper.showOptionsDlg();
              done = true;
/*
            while (!canceled && !done) {
                try {
                    Thread.sleep(1000); //sleep for a second
                    current += Math.random() * 100; //make some progress
                    if (current >= lengthOfTask) {
                        done = true;
                        current = lengthOfTask;
                    }
                    statMessage = "Completed " + current +
                                  " out of " + lengthOfTask + ".";
                } catch (InterruptedException e) {
                    System.out.println("ActualTask interrupted");
                }
            }
*/            
        }
    }
}
