
package it.uniroma2.art.ontoling.protege.enrichment;

import it.uniroma2.art.lw.manager.LinguisticWatermarkManager;
import it.uniroma2.art.lw.model.objects.SemanticIndex;
import it.uniroma2.art.ontoling.protege.OntoLingTab;
import it.uniroma2.art.ontoling.protege.ui.LEnrDialog;
import it.uniroma2.art.ontoling.protege.ui.LinguisticEnrichment_GUI;
import it.uniroma2.art.ale.AlgorithmParameters;
import it.uniroma2.art.ale.LinguisticEnrichmentInterface;
import it.uniroma2.art.ale.StarredLinguisticEnrichment;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Properties;

import javax.swing.JFrame;

import org.apache.commons.collections.IteratorUtils;
import org.apache.commons.collections.iterators.IteratorChain;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntResource;

public class LinguisticEnricherProxy{
	
    OntModel m;
    
	public it.uniroma2.art.ale.LinguisticEnrichmentInterface _LEnr;
 	
    private Jena2Protege2JenaLinguisticEnricherWrapper delegator;
 	private OntResource curResource;                              // l'oggetto Classe selezionata
 	private Iterator classesIterator;          // i nomi delle classi appartenenti all'ontologia
    private Iterator propertiesIterator;                    // i nomi delle propriet� appartenenti all'ontologia
    PrevNextListIterator framesListIterator;
 	private boolean started;
 	
 	public boolean showAS; 
	
 	protected static Log logger = LogFactory.getLog(LinguisticEnricherProxy.class);


    public LinguisticEnricherProxy(OntModel m, Jena2Protege2JenaLinguisticEnricherWrapper delegator){
		this.m = m;
        started=false;
        logger.info("LinguisticEnricherProxy module instantiated.");
        this.delegator=delegator;
	}
	
	public LinguisticEnrichmentInterface getEnricher(){ return _LEnr; }
	
	// enricher: PrologEnricher | LinguisticEnricherProcessInterface
	// it.uniroma2.info.ai_nlp.OntoLingTab.ui.PrologEnricher
	// linguistic.LinguisticEnricherProcessInterface
	void init(){
		
        
        //TODO OntoLing should read from property file or other source which LinguisticEnrichment Implementation to read
        _LEnr = new StarredLinguisticEnrichment();
        
        //INITIALIZATION OF DATA FROM ONTOLING 
        // Ottengo l'istanza della KB e il nome dell'ontologia
        // Costruisco la matrice di plausibilit�


        
        
        //this.setCurClass(jenaOWLModel.getRDFResource((Resource)classesIterator.next()) );
        
//        Util.getCurrentKB().getOWLNamedClass();
                //getCls(ontClasses[0])); // setta come current class la prima della lista, nel caso
                                                                        // in cui viene fatta la scelta 2 o la scelta 3    
        
		//System.out.println("LinguisticInterface: "+LR_Locator.getInstanceOfLinguisticInterface().toString());
        logger.debug(LinguisticWatermarkManager.getSelectedInterface().toString());
        
        String plugin_dir = "plugins/it.uniroma2.info.ai-nlp.ontoling/";
		String onedolldbpath = "";
		String DBname = "";
		
		// Leggo da plugin.properties il valore di Enricher
		Properties ontolingProps = new Properties();
		java.io.FileInputStream fileProps = null;
		try{
			fileProps = new java.io.FileInputStream(new java.io.File(plugin_dir+"plugin.properties"));
		} catch (Exception exce) {System.out.println(exce);}
		try{
			ontolingProps.load(fileProps);}
		catch(Exception exce){System.out.println(exce);}
		
		// Carico dinamicamente l'Enricher prescelto
		String strEnricherClass = ontolingProps.getProperty("LinguisticEnricherClass");
		String storageInterface = ontolingProps.getProperty("StorageInterface");
		String wordNetGlossStatisticsFilePath = ontolingProps.getProperty("WordNetGlossStatistics");
		
		onedolldbpath = ontolingProps.getProperty("OneDollarDBPath");
		DBname = ontolingProps.getProperty("DBName");

	    
	    AlgorithmParameters parameters = new AlgorithmParameters();
	    parameters.addParameter("StorageInterface", storageInterface);
	    if (parameters.getParameter("StorageInterface").equals("it.uniroma2.info.ai_nlp.ontling_enrich.storage.sql.DaffodilDBStorage")) 
	    {
	    	parameters.addParameter("path", onedolldbpath);
	    	parameters.addParameter("databaseName", DBname);
	    }
	    parameters.addParameter("GlossStatisticsFilePath", wordNetGlossStatisticsFilePath);
	    parameters.addParameter("steps", "3");
	    parameters.addParameter("wordFreq", "10000");
	    
	    System.out.println("Stampa dei parametri in ingresso");
	    System.out.println("StorageInterface: " + parameters.getParameter("StorageInterface"));
	    System.out.println("path: " + parameters.getParameter("path"));
	    System.out.println("databaseName: " + parameters.getParameter("databaseName"));
	    System.out.println("GlossStatisticsFilePath: " + parameters.getParameter("GlossStatisticsFilePath"));
	    System.out.println("wordfreq: " + Integer.parseInt(parameters.getParameter("wordFreq")));
	    System.out.println("steps: " + Integer.parseInt(parameters.getParameter("steps")));
	    
	    Date startTime = new Date(); 
	    
	    try {
	    	int steps = Integer.parseInt(parameters.getParameter("steps"));

            //_LEnr.initialize(m, LR_Locator.getInstanceOfLinguisticInterface(), parameters);
	    	_LEnr.initialize(m, LinguisticWatermarkManager.getSelectedInterface(), parameters);
            classesIterator = _LEnr.listEnrichableClasses();
            propertiesIterator = _LEnr.listEnrichableProperties();
            framesListIterator = getFramesIterator();

            setCurResource((OntResource)framesListIterator.next() );
            
            this.started=true;
            
            BufferedWriter out = new BufferedWriter(new FileWriter("PlausibilityMatrixFile.pl"));
            ((StarredLinguisticEnrichment)_LEnr).exportConceptSynsetEvidences(out);
            out.close();
			int visualStep=Integer.parseInt(parameters.getParameter("steps"));
			int step=visualStep-1;
			visualStep=visualStep-(step);
			/*while(steps>0){
				System.out.println("  *************** Plausibility Matrix Evolution. Step: "+visualStep+" *****************");
				_LEnr.updateSensesPlausibility();
	            out = new BufferedWriter(new FileWriter("SynPlausDB_"+visualStep+".pl"));
	            ((StarredLinguisticEnrichment)_LEnr).exportConceptSynsetPlausibilities(visualStep, out);
	            out.close();
				visualStep++;
				steps--;
			}*/
			_LEnr.close();
        } catch (Exception e1) {
            e1.printStackTrace();
        }
	    Date endTime = new Date();
	    System.out.println(	(((double)endTime.getTime() - (double)startTime.getTime())/60000)	);
		
	}
	
	
    /**
     * @author Armando Stellato
     * This class wraps a ListIterator, providing different implementations of its methods next and prev
     * A List iterator has its index situated between next and prev visited elements, so that subsequent calls to:
     * 1) next 2) prev (or the converse)
     * result in the "prev" being stopped at the same location which has been pointed by next.
     * This implementation offers more friendly behaviour of prev and next methods.
     */
    private class PrevNextListIterator implements ListIterator {

        private ListIterator it;
        private int BCK = -1;
        private int FWD = +1;
        private int NOPREVMOVE = 0;
        private int lastMove;
        
        /**
         * @param it
         */
        public PrevNextListIterator(ListIterator it) {
            this.it = it;
            lastMove = NOPREVMOVE;
        }
        
        public void add(Object arg0) {
            it.add(arg0);
        }

        public boolean hasNext() {
            return it.hasNext();
        }

        public boolean hasPrevious() {
            return it.hasPrevious();
        }

        public Object next() {
            if (lastMove!=BCK) 
                {System.out.println("next1"); lastMove=FWD; return it.next();}
            else 
                {System.out.println("next2"); lastMove=FWD; it.next(); return it.next();}  //skip previously visited element
        }

        public int nextIndex() {
            if (lastMove!=BCK) return it.nextIndex();
            else return it.nextIndex()+1;  //skip previously visited element
        }

        public Object previous() {
            if (lastMove!=FWD)
                { lastMove=BCK; return it.previous(); }
            else
                { lastMove=BCK; it.previous(); return it.previous();}  //skip previously visited element

        }

        public int previousIndex() {
            if (lastMove!=FWD) return it.previousIndex();
            else return it.previousIndex() - 1;  //skip previously visited element
        }

        public void remove() {
            it.remove();
        }

        public void set(Object arg0) {
            it.set(arg0);
        }
        
    }
    
    
    private PrevNextListIterator getFramesIterator() {
        ArrayList frameIterators = new ArrayList();
        frameIterators.add(_LEnr.listEnrichableClasses());
        frameIterators.add(_LEnr.listEnrichableProperties());
        IteratorChain framesIterator = new IteratorChain(frameIterators);
        return new PrevNextListIterator(IteratorUtils.toListIterator(framesIterator));
    }
    
    
	public String toString(){
		return "LinguisticEnricherProxy.Object";
	}
		
	/**
	 * Says whether the LinguisticEnricherProxy has been started (NOT instantiated)
	 * or not.<BR>At present time, 'started' means that the plausibility matrix has been built.
	 * @return True if the LinguisticEnricherProxy has been started, False otherwise
	 */
	boolean hasStarted(){
		return started;
	}
	
	/**
	 * Sets the current Class as current frame.
	 * @param cclass The Cls object to set as current frame
	 */
    void setCurResource(OntResource res){
     	curResource = res;
    }

    
	/**
	 * Get the current Cls object being examined
	 * @return the current Cls object
	 * 
	 * @uml.property name="curClass"
	 */
	OntResource getCurResource() {
		return curResource;
	}
  
     
 	/**
 	 * Get the framename (which can be either a class or a property) for the frame
 	 * following the current one.
 	 * @return The frame name
 	 */
     OntResource nextResource(){
         return (OntResource)framesListIterator.next();
     }
     
     
  	/**
  	 * Get the framename (which can be either a class or a properties) for the frame
  	 * preceding the current one.
  	 * @return The frame name
  	 */
     OntResource prevResource(){
         return (OntResource)framesListIterator.previous();
     }
     

     boolean hasPrevResource(){
         return framesListIterator.hasPrevious();
     }

     
     boolean hasNextResource(){
         return framesListIterator.hasNext();
     }

     
	/**
	 * Get the current step in the iterative process of enrichment.
	 * @return the current step
	 */
	int getCurStep() {
		return _LEnr.getCurrentStep();
	}

     
    private static JFrame optionsDlg=null;

	/**
	 * 
	 * @uml.property name="lenrDlg"
	 * @uml.associationEnd multiplicity="(0 1)"
	 */
	private static LEnrDialog lenrDlg = null;

     /**
      * Open up the three-options dialog.<BR>
      * This dialog gives three separate choices to the user, each of which
      * follows a different approach in the linguistic enrichment process.
      */
     void showOptionsDlg(){
     	if (optionsDlg==null){
	 		optionsDlg = new javax.swing.JFrame();
	 		lenrDlg=new LEnrDialog();
     	}
     		optionsDlg.setSize(355,150);
     		optionsDlg.getContentPane().setSize(new java.awt.Dimension(355,150));
     		optionsDlg.setResizable(false);
	 		optionsDlg.getContentPane().add(lenrDlg);
	 		optionsDlg.getContentPane().setVisible(true);
	 		optionsDlg.setLocationRelativeTo(null);
	 		optionsDlg.setTitle("Linguistic Enrichment");
     		optionsDlg.setVisible(true);
     		optionsDlg.transferFocus();
    }

     /**
      * Run a completely automatic enrichment for the current ontology.<BR>
      * Only AS (Automatically Selected) senses are taken under consideration for
      * each class, and the ontology is enriched with glosses and terms with no
      * user-interaction at all.<BR>
      * Senses which were already added to a class at a certain step are no longer
      * requested by the next steps.
      *
      */
     void enrichAutomatic(){

     	LinkedList listSenses;
        Iterator senseIterator;
     	
        Iterator framesAutomaticListIterator = getFramesIterator();
       
        while(framesAutomaticListIterator.hasNext()) {
     		
            OntResource res = (OntResource)framesAutomaticListIterator.next();
            logger.debug("yet visited OntResource: " + res);
     		listSenses = _LEnr.getSensesForFrame(res,getCurStep(),true);
     		senseIterator = listSenses.iterator();
     		
     		while(senseIterator.hasNext()){
     			
     			// ottengo il sense id
     			SemanticIndex sense = (SemanticIndex)senseIterator.next();
     			// controllo se il frame � stato gi� arricchito con questo senso in uno step precedente
     			if (!_LEnr.wasAlreadyAssigned(res,sense)) {
     				// faccio l'assert dell'arricchimento del frame corrente col senso corrente
     				//strPlausibility = _LEnr.getPlausibilityAtStep(res, sense, getCurStep());
     				_LEnr.assertEnrichment(res,sense,getCurStep());
     				// faccio l'arricchimento
                    logger.debug("Arricchisco "+res+" con "+sense+", plaus: "+_LEnr.getPlausibilityAtStep(res, sense, getCurStep()));
                    delegator.doEnrichment(res, sense);
     			}
     		}
     		
     	}
     }
     
 
     private static JFrame frame=null;
     
     /**
      * Open up the Table dialog.<BR>
      * This dialog contains, for the current frame, a list of senses, each of which valued with
      * a plausibility between 0 and 1.
      */
     LinguisticEnrichment_GUI showTableDialog() {
        LinguisticEnrichment_GUI tabellina = new LinguisticEnrichment_GUI(frame);
 		if (frame==null) frame = new JFrame("Current Frame: "+getCurResource().getLocalName());
 		frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE); // disabilita il pulsante di chiusura finestra!
 		frame.setTitle("Current Frame: '"+getCurResource()+"'");
 		frame.setSize(new java.awt.Dimension(490,460));
 		frame.getContentPane().setSize(new java.awt.Dimension(490,460));
 		frame.setResizable(false);
 		frame.getContentPane().add(tabellina);
 		frame.setLocationRelativeTo(null);
 		frame.pack();
 		frame.setVisible(true);
        return tabellina;
     }
     

     // Elimina il termine term dall'array arTerms e ritorna l'array pulito
     //TODO
     private String[] purgeTerms(String term,String arTerms[]){
     	return arTerms;
     }
     
     
     /**
      * Compute next step plausibilities.<BR>
      * This method wraps the interface method updateSensesPlausibility for computing
      * senses plausibilities step-by-step.
      */
     void doNextStep(){
		// Calcolo delle plausibilit� step successivo 
     	if (!canDoNextStep()) return;
        logger.debug("performing step: " + _LEnr.getCurrentStep());
		_LEnr.updateSensesPlausibility(_LEnr.getCurrentStep()+1);
     }
     
     LEnrDialog getLEnrDialog(){
     	return lenrDlg;
     }
     
     /**
      * Says whether the next step can be performed ot not.<BR>
      * Usually the next step cannot be performed if the Table dialog is currently open.
      * @return TRUE if the next step can be performed, FALSE otherwise. 
      */
     boolean canDoNextStep(){
     	return !(frame!=null && frame.isVisible());
     }
     
    /**
      * @return the showAS
      */
     boolean isShowAS() {
         return showAS;
     }

     /**
      * @param showAS the showAS to set
      */
     void setShowAS(boolean showAS) {
         this.showAS = showAS;
     }

}