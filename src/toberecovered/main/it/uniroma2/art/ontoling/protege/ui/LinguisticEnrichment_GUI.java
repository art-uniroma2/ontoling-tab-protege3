 
 package it.uniroma2.art.ontoling.protege.ui;

import it.uniroma2.art.lw.manager.LinguisticWatermarkManager;
import it.uniroma2.art.lw.model.objects.SemanticIndex;
import it.uniroma2.art.lw.model.objects.SemIndexRetrievalException;
import it.uniroma2.art.lw.model.objects.ConceptualizedLR;
import it.uniroma2.art.lw.model.objects.LRWithGlosses;
import it.uniroma2.art.ontoling.OntoLingCore;
import it.uniroma2.art.ontoling.protege.OntoLingTab;
import it.uniroma2.art.ontoling.protege.enrichment.Jena2Protege2JenaLinguisticEnricherWrapper;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.Logger;

import edu.stanford.smi.protege.model.Cls;
import edu.stanford.smi.protege.model.Slot;
import edu.stanford.smi.protegex.owl.model.RDFResource;


 public class LinguisticEnrichment_GUI extends javax.swing.JPanel {

	/**
	 * 
	 * @uml.property name="lenr"
	 * @uml.associationEnd multiplicity="(0 1)"
	 */
	private Jena2Protege2JenaLinguisticEnricherWrapper lenrDelegate;
	private ConceptualizedLR lInst;
    private JFrame hostingFrame;
    
    public static Color GREEN = new Color(169,236,154);
    public static Color LIGHT_RED = new Color(253,208,208);
    public static Color CELESTIAL = new Color(213,231,255);
    public static Color LIGHT_GREEN = new Color(200,243,190);
    
    public int HighestPlausibilityRowIndex;           // la riga che contiene il concetto con pi� alta plausibilit�
    public String HighestPlausibilityStr;             // il concetto con pi� alta plausibilit�
    public int LowestPlausibilityRowIndex;
    public String LowestPlausibilityStr;
    public GUI_Facade guiFacade;
    public OntologyPanel ontPanel;
    ClsesPanel clsPanel;
    SubslotPane subSlotPanel;
    
     
    protected static Log logger = LogFactory.getLog(LinguisticEnrichment_GUI.class);
    
     /** Creates new form LinguisticEnrichment_GUI */
     public LinguisticEnrichment_GUI(JFrame host) {
         OntoLingCore olCore = OntoLingCore.getOntoLingCore();
         ontPanel = GUI_Manager.getOntologyPanel();
         clsPanel = ontPanel.getInstanceOfClsesPanel();
         subSlotPanel = ontPanel.getInstanceOfSubslotPane();
         guiFacade = OntoLingTab.getOntoLingTab().getGUIFacade();         
         hostingFrame = host;
     	 loadLEnr();
         lInst = (ConceptualizedLR)LinguisticWatermarkManager.getSelectedInterface();
         initComponents();
         btnConfirm.setEnabled(false);
         RDFResource currFrame = lenrDelegate.getCurResource();
         showConceptsForFrame(currFrame,lenrDelegate.isShowAS());
         
         if (!lenrDelegate.hasNextResource()) btnNextFrame.setEnabled(false);
         if (!lenrDelegate.hasPrevResource()) btnPrevFrame.setEnabled(false);
     	 if (lenrDelegate.isClass(currFrame)) {
     	    GUI_Manager.getOntologyPanel().getInstanceOfClsesPanel().setExpandedCls((Cls)lenrDelegate.getCurResource(),true);
     	    GUI_Manager.getOntologyPanel().getInstanceOfClsesPanel().setSelectedCls((Cls)lenrDelegate.getCurResource());
         }
     }
     
     private void loadLEnr(){
        lenrDelegate = EnrichmentPanel.getInstanceOfLinguisticEnricherWrapper();
     }
     
     /**
      * Show in table the concepts, glosses and plausibilities for the given ontology frame.
      * @param currentFrame The Frame (Class or Slot) for which the concepts are
      * displayed.
      * @param AutomaticallySelected Set to TRUE if you want to show only concepts
      * above the Automatically Selected Treshold, FALSE otherwise.
      */
     public void showConceptsForFrame(RDFResource currentFrame,boolean AutomaticallySelected){
     	SemanticIndex semex;
     	boolean empty=false;
     	
     	java.util.LinkedList lLista = new java.util.LinkedList();
     	lLista = lenrDelegate.getSensesForFrame(currentFrame,lenrDelegate.getCurStep(),AutomaticallySelected);
     	if (lLista.isEmpty()) empty=true;
     	
		((ResultsTableModel_LE)(ResultsTable.getModel())).clear();
		//ResultsTable.repaint();
		
		Iterator it = lLista.iterator();
		ArrayList ar = new ArrayList();
		
		
		while (it.hasNext()){
			semex = (SemanticIndex)it.next();
			ar.add(new SemexPlausibilityPair (semex, lenrDelegate.getPlausibilityAtStep(currentFrame, semex, lenrDelegate.getCurStep()) ) );
						//			((LRWithGlosses)lInst).getConceptGloss(semex),
		}
		
		System.out.println(ar);
		Collections.sort(ar);
		System.out.println(ar);
		it = ar.iterator();
		
		while (it.hasNext()){
			semex = ( (SemexPlausibilityPair)it.next() ).getSemex();
			addSensePlaus(semex,((LRWithGlosses)lInst).getConceptGloss(semex),lenrDelegate.getPlausibilityAtStep(currentFrame, semex, lenrDelegate.getCurStep()));
		}
 
		//addSensePlaus(semex,lenrDelegate.getPlausibilityAtStep(currentFrame, semex, lenrDelegate.getCurStep()));
		
		// Cerco il concetto con pi� alta plausibilit�
		if (!empty) orderTable();
		else { HighestPlausibilityRowIndex=-1;
        LowestPlausibilityRowIndex=-1; 
		}
     }
     
     private class SemexPlausibilityPair implements Comparable {

    	private SemanticIndex semex;
    	private double plausibility;
    	    	
    	public SemexPlausibilityPair(SemanticIndex semex, double plausibility) {
    		this.semex = semex;
    		this.plausibility = plausibility;
    	}
    	
    	public double getPlausibility() {
    		return plausibility;
    	}
    	
    	public SemanticIndex getSemex() {
    		return semex;
    	}
    	
		public int compareTo(Object arg0) {
			logger.debug("comparing two SemexPlausibilityPairs: " + plausibility + " vs " + ((SemexPlausibilityPair)arg0).getPlausibility() + ": " + (int)(((SemexPlausibilityPair)arg0).getPlausibility()*100 - plausibility*100  ) );
			return (int)( ((SemexPlausibilityPair)arg0).getPlausibility()*100 - plausibility*100 );
		}

     }
     
     /**
      * Adds a row in table with ID, gloss and plausibility for the given LR-concept.
      * @param semex The sense ID for the concept to add in table.
      * @param plausibility The plausibility value for the concept to add in table.
      */
     public void addSensePlaus(SemanticIndex semex, String gloss, double plausibility){
     	//String senseID = SenseID.substring(1,SenseID.length()-1);
     	((ResultsTableModel_LE)ResultsTable.getModel()).addRow(new String [] {semex.toString(),gloss,Double.toString(plausibility)} );     	
     }
     
     /**
      * Refresh the GUI by repainting the table and showing correct concepts
      * for the current frame.
      */
     public void refresh(){
     	btnConfirm.setEnabled(false);
     	showConceptsForFrame(lenrDelegate.getCurResource(),lenrDelegate.isShowAS());
     	ResultsTable.repaint();
     }

     /**
      * Orders the table on the Plausibility field.
      * TODO
      */
     public void orderTable(){
     	// aggiorna i parametri relativi al/ai senso/i con plausibilit� pi� elevata
        HighestPlausibilityRowIndex = findMaxPlausibility();
        HighestPlausibilityStr = ResultsTable.getValueAt(HighestPlausibilityRowIndex,2).toString();
     	//lenr.LowestPlausibilityRowIndex = findMinPlausibility();
     	//lenr.LowestPlausibilityStr = ResultsTable.getValueAt(lenr.LowestPlausibilityRowIndex,2).toString();
     	// vengono colorate di verde le righe coi concetti con pi� alta plausibilit� per la classe corrente
     	ResultsTable.repaint();
     }
     
     /**
      * Finds the maximum plausibility in the concept/gloss/plaus table.
      * @return The row index for the concept which has the maximum plausibility.
      */
     public int findMaxPlausibility(){
     	Double valueatrow; int row;
     	row=0;
     	valueatrow = Double.valueOf(ResultsTable.getValueAt(0,2).toString());
     	for (int i=0; i<ResultsTable.getRowCount();i++){
     		if (Double.valueOf(((String)(ResultsTable.getValueAt(i,2)))).doubleValue()>valueatrow.doubleValue()){
     			valueatrow = Double.valueOf(((String)(ResultsTable.getValueAt(i,2))));
     			row=i;
     		}
     	}
     	return row;
     }
     
     /**
      * Finds the minimum plausibility in the concept/gloss/plaus table.
      * @return The row index for the concept which has the minimum plausibility.
      */
     public int findMinPlausibility(){
     	Double valueatrow; int row;
     	row=0;
     	valueatrow = Double.valueOf(ResultsTable.getValueAt(0,2).toString());
     	for (int i=0; i<ResultsTable.getRowCount();i++){
     		if (Double.valueOf(((String)(ResultsTable.getValueAt(i,2)))).doubleValue()<valueatrow.doubleValue()){
     			valueatrow = Double.valueOf(((String)(ResultsTable.getValueAt(i,2))));
     			row=i;
     		}
     	}
     	return row;
     }
          
     // Per colorare di verde la riga che contiene il senso
     // a plausibilit� pi� elevata degli altri e impostare alcune cose alla tabella
     class RenderPlaus extends DefaultTableCellRenderer {
     	  
     	  RenderPlaus () {
     	    setHorizontalAlignment(SwingConstants.RIGHT);  
     	  }
     	  
     	  public Component getTableCellRendererComponent(JTable aTable, 
     	                                          Object aNumberValue, 
     	                                          boolean aIsSelected, 
     	                                          boolean aHasFocus, 
     	                                          int aRow, int aColumn) {  

     	    if (aNumberValue == null) return this;
     	    
     	    java.awt.Component renderer = super.getTableCellRendererComponent(aTable, 
     	                                                       aNumberValue, 
     	                                                       aIsSelected, 
     	                                                       aHasFocus, 
     	                                                       aRow, aColumn);
     	    // controlla se il concetto � stato gi� assegnato in precedenza
     	    SemanticIndex selectedSemex=null;
     	    try {
     	        selectedSemex = lInst.getConcept(ResultsTable.getValueAt(aRow,0).toString());
     	    } catch (SemIndexRetrievalException e) {
     	        // TODO Auto-generated catch block
     	        e.printStackTrace();
     	    }
            logger.debug("testing RenderPlaus: curResource" + lenrDelegate.getCurResource() + "selectedSemex: " + selectedSemex);
     	    if (lenrDelegate.wasAlreadyAssigned( lenrDelegate.getCurResource(), selectedSemex )  ) {
     	    	setOpaque(true);
     	    	renderer.setBackground(CELESTIAL);
     	    }
     	    // colora le righe
     	    else if (HighestPlausibilityRowIndex!=-1) {
	     	    // plausibilit� >= AST
	     	    if (java.lang.Float.parseFloat(ResultsTable.getValueAt(aRow,2).toString())>=lenrDelegate.getEnricher().getAST()) {
	       	      setOpaque(true);
	     	      renderer.setBackground(GREEN);
	     	    } 
	     	    // plausibilit� < AST  &&  plausibilit� max
	     	    else if (HighestPlausibilityStr.equals(ResultsTable.getValueAt(aRow,2)) &&
	     	    		java.lang.Float.parseFloat(ResultsTable.getValueAt(aRow,2).toString())<lenrDelegate.getEnricher().getAST()) {
	       	      setOpaque(true);
	     	      renderer.setBackground(LIGHT_GREEN);
	     	    }
	     	    // plausibilit� <= ADT
	     	    else if (java.lang.Float.parseFloat(ResultsTable.getValueAt(aRow,2).toString())<=lenrDelegate.getEnricher().getADT()) {
	       	      setOpaque(true);
	     	      renderer.setBackground(LIGHT_RED);
		     	    }
	     	    else {
	       	      setOpaque(false);
	     	      renderer.setBackground(Color.white);
	     	    }
     	    }

     	    if (aColumn == 0 || aColumn==1)
     	    	setHorizontalAlignment( LEFT );
     	    else if (aColumn == 2) {
     	    	setHorizontalAlignment( RIGHT );
     	    }
     	    return this;
     	  }
     	} 
     
     
     private void initComponents() {
     	
     	loadLEnr();

         SearchResults = new javax.swing.JPanel();
         Pulsanti = new JPanel();
         jScrollPane1 = new javax.swing.JScrollPane();
         ResultsTable = new javax.swing.JTable();
         
         spazio = new JPanel();
         
         btnNextFrame = new JButton(); // pulsante Next Step
         btnPrevFrame = new JButton(); // pulsante Prev Step
         btnConfirm = new JButton();   // pulsante Enrich selected class..
         btnClose = new JButton("Close");
         
         ResultsTable.setDefaultRenderer(Object.class,new RenderPlaus());
         
         btnNextFrame.setLabel("Next Frame");
         btnPrevFrame.setLabel("Prev Frame");
         btnConfirm.setLabel("Confirm");

         setLayout(new javax.swing.BoxLayout(this, javax.swing.BoxLayout.X_AXIS));
         setSize(200,150);

         SearchResults.setLayout(new javax.swing.BoxLayout(SearchResults, javax.swing.BoxLayout.Y_AXIS));
         SearchResults.setBorder(new javax.swing.border.EmptyBorder(new java.awt.Insets(10, 10, 10, 10)));
         SearchResults.setPreferredSize(new java.awt.Dimension(400, 300));
         
         Pulsanti.setLayout(new javax.swing.BoxLayout(Pulsanti, javax.swing.BoxLayout.Y_AXIS));
         Pulsanti.setMaximumSize(new java.awt.Dimension(85, 275));
         
         jScrollPane1.setPreferredSize(new java.awt.Dimension(400, 200));
         
         ResultsTable.setFont(new java.awt.Font("MS Sans Serif", 0, 10));
         ResultsTable.setModel(new ResultsTableModel_LE());
         ResultsTable.setToolTipText("Results Table: left click or press SPACE to select a result");
         ResultsTable.setMaximumSize(new java.awt.Dimension(600, 100));
         ResultsTable.getColumnModel().getColumn(0).setPreferredWidth(85);
         ResultsTable.getColumnModel().getColumn(0).setMinWidth(40);
         ResultsTable.getColumnModel().getColumn(1).setPreferredWidth(270);
         ResultsTable.getColumnModel().getColumn(2).setPreferredWidth(65);
         ResultsTable.addMouseListener(new java.awt.event.MouseAdapter() {
             public void mouseClicked(java.awt.event.MouseEvent evt) {
             	btnConfirm.setEnabled(true);
             	String selsense = (String)ResultsTable.getModel().getValueAt(ResultsTable.getSelectedRow(), 0);
                String selWord = (String)ResultsTable.getModel().getValueAt(ResultsTable.getSelectedRow(), 1);
             	if (evt.getClickCount() == 1){
                     guiFacade.notifiedSelectedRowOnResultsTable(selsense, selWord);
                 }
             }
         });
         ResultsTable.addKeyListener(new KeyAdapter() {
             public void keyPressed(KeyEvent ke) {  
                 int kc = ke.getKeyCode();
                 if (kc == KeyEvent.VK_SPACE) {
                	 String selsense = (String)ResultsTable.getModel().getValueAt(ResultsTable.getSelectedRow(), 0);
                	 String selWord = (String)ResultsTable.getModel().getValueAt(ResultsTable.getSelectedRow(), 1);
                	 try {guiFacade.notifiedSelectedRowOnResultsTable(selsense, selWord);  }
                     catch(Exception e) {System.out.println(e);};
                 }
             }   
         });

         
         btnNextFrame.addActionListener(new ActionListener() {
         	public void actionPerformed(ActionEvent e){
         			changeFrame(lenrDelegate.nextResource());
                    if (!lenrDelegate.hasNextResource())
                        btnNextFrame.setEnabled(false);
         	}
         });
         
         
         btnPrevFrame.addActionListener(new ActionListener() {
         	public void actionPerformed(ActionEvent e){
     			changeFrame(lenrDelegate.prevResource());
                if (!lenrDelegate.hasPrevResource())
                    btnPrevFrame.setEnabled(false);
         	}
         });
         
         
         btnConfirm.addActionListener(new ActionListener() {
         	public void actionPerformed(ActionEvent e){
                try {
                    lenrDelegate.doEnrichment(
                                    lenrDelegate.getCurResource(),
                                    lInst.getConcept(ResultsTable.getValueAt(ResultsTable.getSelectedRow(),0).toString())
                                 );
                } catch (SemIndexRetrievalException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
         		btnConfirm.setEnabled(false);
         		ResultsTable.repaint();
         	}
         });
         
         btnClose.addActionListener(new ActionListener() {
         	public void actionPerformed(ActionEvent e){
         		setVisible(false);
         		((JFrame)(getRootPane().getParent())).setVisible(false);
         		EnrichmentPanel.enableNextStep();
                lenrDelegate.showOptionsDlg();
         	}
         });
         
         jScrollPane1.setViewportView(ResultsTable);
         SearchResults.add(jScrollPane1);
         
         Pulsanti.add(btnPrevFrame);
         Pulsanti.add(btnNextFrame);
         
         int h = btnConfirm.getPreferredSize().height;
         int sp_h = Pulsanti.getMaximumSize().height - (4*h) -10;
         
 		spazio.setMinimumSize(new Dimension(5,sp_h));
		spazio.setMaximumSize(new Dimension(5,sp_h));
		spazio.setPreferredSize(new Dimension(5,sp_h));
		//spazio.setBorder(new LineBorder(Color.GREEN,1));
		
		Pulsanti.add(spazio);
         
         Pulsanti.add(btnConfirm);
         Pulsanti.add(btnClose);

         add(SearchResults);
         add(Pulsanti,java.awt.BorderLayout.EAST);

     }
     public void changeFrame(RDFResource curFrame) {
				
            if (lenrDelegate.isClass(curFrame)) {
                ontPanel.swapTab(0);
                clsPanel.setExpandedCls((Cls)curFrame,true);
                clsPanel.setSelectedCls((Cls)curFrame);
            }
            else if (lenrDelegate.isProperty(curFrame)) {
                ontPanel.swapTab(1);
                subSlotPanel.setExpandedSlot((Slot)curFrame,true);
                subSlotPanel.setSelectedSlot((Slot)curFrame);
            } 
            
            lenrDelegate.setCurResource(curFrame);

            showConceptsForFrame(curFrame,lenrDelegate.isShowAS());
     		btnConfirm.setEnabled(false);
     		((JFrame)getRootPane().getParent()).setTitle("Current Frame: '"+lenrDelegate.getCurResource().getBrowserText()+"'");
     		refresh();
     		btnNextFrame.setEnabled(true);
     		btnPrevFrame.setEnabled(true);
     		//the if in the action for the two buttons will then grey one of the two if the array is close.
     }
     
     public JFrame getHostingFrame() {
        return hostingFrame;
    }
     
     private javax.swing.JTable ResultsTable;
     private javax.swing.JPanel SearchResults;
     private javax.swing.JScrollPane jScrollPane1;
     private JButton btnNextFrame;
     private JButton btnPrevFrame;
     private JButton btnConfirm;
     private JButton btnClose;
     private JPanel Pulsanti;
     private JPanel spazio;
     // End of variables declaration//GEN-END:variables
     
 }
