package it.uniroma2.art.ontoling.protege.ui;

import it.uniroma2.art.ontoling.protege.enrichment.Jena2Protege2JenaLinguisticEnricherWrapper;

import javax.swing.*;
//import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class LEnrDialog extends JPanel {
	
	private static final long serialVersionUID = -7928443686904424605L;
	private JButton btnOK = new JButton("OK");
	private JButton btnNextStep = new JButton("Next Step");
	private static JLabel lblCurStep = new JLabel();
	private JPanel pnlOptions = new JPanel();
	private JPanel pnlCommands = new JPanel();
	private JPanel pnlButtons = new JPanel();
	private ButtonGroup grpOptions = new ButtonGroup();
	private JRadioButton optAuto = new JRadioButton("Enrich all classes automatically");
	private JRadioButton optHuman = new JRadioButton("Enrich classes manually");
    private LinguisticEnrichment_GUI enrichmentGUI;

	/**
	 * 
	 * @uml.property name="lenr"
	 * @uml.associationEnd multiplicity="(0 1)"
	 */
	private Jena2Protege2JenaLinguisticEnricherWrapper lenrDelegate;

	
	
	public LEnrDialog(){
		lenrDelegate = EnrichmentPanel.getInstanceOfLinguisticEnricherWrapper();
		initialize_components();
	}
	
	private void loadLEnr(){
        lenrDelegate = EnrichmentPanel.getInstanceOfLinguisticEnricherWrapper();
	}
	
	/**
	 * Refresh the GUI by updating the label for the current step.
	 * @param step The new step.
	 */
	public static void updateCurStepLabel(int step){
		lblCurStep.setText("Current Step: "+step);
	}
	
	private void initialize_components(){
		
		loadLEnr();
		
		// Set-up options
		optAuto.setSelected(true);
		
		// Set-up labels
		lblCurStep.setText("Current Step: "+lenrDelegate.getCurStep());
		
		// Set-up buttongroup
        grpOptions.add(optHuman);
        grpOptions.add(optAuto);

		
		// Set-up pannello pulsanti
		pnlButtons.setLayout(new BoxLayout(pnlButtons,BoxLayout.LINE_AXIS));
		//pnlButtons.setBorder(new LineBorder(Color.RED,1));
		pnlButtons.add(btnOK);
		pnlButtons.add(btnNextStep);
		
		// Set-up pannello comandi
		pnlCommands.setLayout(new GridBagLayout());
		pnlCommands.setMaximumSize(new Dimension(310,35));
		pnlCommands.setMinimumSize(new Dimension(310,35));
		//pnlCommands.setBorder(new LineBorder(Color.BLACK,1));
		
		GridBagConstraints gridBag = new GridBagConstraints();
		gridBag.fill = GridBagConstraints.HORIZONTAL;
		
		gridBag.anchor = java.awt.GridBagConstraints.LINE_START;
		pnlCommands.add(lblCurStep,gridBag);
		
		JPanel spazio = new JPanel();
		spazio.setMinimumSize(new Dimension(70,5));
		spazio.setMaximumSize(new Dimension(70,5));
		spazio.setPreferredSize(new Dimension(70,5));
		//spazio.setBorder(new LineBorder(Color.GREEN,1));
		gridBag.anchor = GridBagConstraints.CENTER;
		pnlCommands.add(spazio,gridBag);
		
		gridBag.anchor = GridBagConstraints.LINE_END;
		pnlCommands.add(pnlButtons,gridBag);
		
		// Set-up pannello opzioni
        pnlOptions.setLayout(new javax.swing.BoxLayout(pnlOptions, javax.swing.BoxLayout.Y_AXIS));
        pnlOptions.setMinimumSize(new Dimension(310,100));
        pnlOptions.setMaximumSize(new Dimension(310,100));
        pnlOptions.setBorder(new TitledBorder(null, "Enrichment Options", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("MS Sans Serif", 1, 12)));
        pnlOptions.add(optAuto);
        pnlOptions.add(optHuman);
        
        // Disegna il form
        setLayout(new GridBagLayout());
        setSize(new Dimension(345,150));
        gridBag.gridy = 0;
        add(pnlOptions,gridBag);
        gridBag.gridy = 1;
        gridBag.insets = new Insets(7,0,0,0);
        add(pnlCommands,gridBag);
        
        // Gestione eventi
		doEvents();
	}
	
	private void doEvents(){
		
		loadLEnr();
		
        btnOK.addActionListener(new ActionListener() {
         	public void actionPerformed(ActionEvent e){
         		// Arricchimento completamente automatico
         		if (optAuto.isSelected()){
         			//setVisible(false);
         			((JFrame)(getRootPane().getParent())).setVisible(false);
                    lenrDelegate.enrichAutomatic();
         			JOptionPane.showMessageDialog(null, "Automatic Enrichment done!");
         			lblCurStep.setText("Current Step: "+lenrDelegate.getCurStep());
         			setVisible(true);
         			((JFrame)(getRootPane().getParent())).setVisible(true);
         		}
         		// Arricchimento completamente manuale
         		else if (optHuman.isSelected()){
         			//setVisible(false);
         			((JFrame)(getRootPane().getParent())).setVisible(false);
                    lenrDelegate.setShowAS(false);
                    enrichmentGUI = lenrDelegate.showTableDialog();
         		}
         		else {
         		// qui non dovrebbe mai capitarci..
         			JOptionPane.showMessageDialog(null,
         				    "Please choose one of the three options first!",
         				    "Choose an option",
         				    JOptionPane.WARNING_MESSAGE);
         		}
         		//ClsesPanel.disableNextStep();
         	}
         });
		
        btnNextStep.addActionListener(new ActionListener() {
         	public void actionPerformed(ActionEvent e){
               lenrDelegate.doNextStep();
         		lblCurStep.setText("Current Step: "+lenrDelegate.getCurStep());
         	}
         });
		
	}
	
    public LinguisticEnrichment_GUI getEnrichmentGUI() {
        return enrichmentGUI;
    }
}