 /*
  * The contents of this file are subject to the Mozilla Public License
  * Version 1.1 (the "License");  you may not use this file except in compliance
  * with the License.  You may obtain a copy of the License at
  * http//www.mozilla.org/MPL/
  *
  * Software distributed under the License is distributed on an "AS IS" basis,
  * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
  * the specific language governing rights and limitations under the License.
  *
  * The Original Code is ONTOLING_2.0.
  *
  * The Initial Developer of the Original Code is University of Roma Tor Vergata.
  * Portions created by University of Roma Tor Vergata are Copyright (C) 2004.
  * All Rights Reserved.
  *
  * ONTOLING_2.0 was developed by the Artificial Intelligence Research Group
  * (ai-nlp.info.uniroma2.it) at the University of Roma Tor Vergata
  * Current information about ONTOLING_2.0 can be obtained at 
  * http//ai-nlp.info.uniroma2.it/software/...
  *
  */

  /*
   * Contributor(s): Armando Stellato stellato@info.uniroma2.it
  */
package it.uniroma2.art.ontoling.protege.enrichment;

import it.uniroma2.art.ale.LinguisticEnrichmentInterface;
import it.uniroma2.art.lw.manager.LinguisticWatermarkManager;
import it.uniroma2.art.lw.model.objects.ConceptualizedLR;
import it.uniroma2.art.lw.model.objects.LRWithGlosses;
import it.uniroma2.art.lw.model.objects.LinguisticInterface;
import it.uniroma2.art.lw.model.objects.SemanticIndex;
import it.uniroma2.art.ontoling.protege.OntoLingTab;
import it.uniroma2.art.ontoling.protege.ui.GUI_Facade;
import it.uniroma2.art.ontoling.protege.ui.LEnrDialog;
import it.uniroma2.art.ontoling.protege.ui.LinguisticEnrichment_GUI;

import java.lang.reflect.InvocationTargetException;
import java.util.LinkedList;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.ontology.OntModelSpec;
import com.hp.hpl.jena.ontology.OntResource;
import com.hp.hpl.jena.rdf.model.ModelFactory;
import com.hp.hpl.jena.reasoner.ReasonerRegistry;

import edu.stanford.smi.protege.model.Frame;
import edu.stanford.smi.protege.ui.ProjectManager;
import edu.stanford.smi.protegex.owl.jena.JenaOWLModel;
import edu.stanford.smi.protegex.owl.model.RDFResource;

public class Jena2Protege2JenaLinguisticEnricherWrapper {
    
    private JenaOWLModel jenaOWLModel;
    private LinguisticEnricherProxy lEnr;
    private OntModel m;
    private LinguisticEnrichmentInterface lEnrInt; 
    protected static Log logger = LogFactory.getLog(Jena2Protege2JenaLinguisticEnricherWrapper.class);
    private GUI_Facade guiFacade;

    /**
     * @param enr
     */
    public Jena2Protege2JenaLinguisticEnricherWrapper() {
        jenaOWLModel = (JenaOWLModel)ProjectManager.getProjectManager().getCurrentProject().getKnowledgeBase();
        OntModelSpec spec = new OntModelSpec( OntModelSpec.OWL_DL_MEM );
        spec.setReasoner(ReasonerRegistry.getTransitiveReasoner());
        m = ModelFactory.createOntologyModel(spec, jenaOWLModel.getOntModel());
        lEnr = new LinguisticEnricherProxy(m, this);
        guiFacade = OntoLingTab.getOntoLingTab().getGUIFacade();
    }
    
    public void init() {
        lEnr.init();
        lEnrInt = lEnr.getEnricher();
        System.out.println("linguistic Enricher interface (from constructor): " + lEnrInt);        
    }

    public OntResource getCurrentEnrichingResource() {
        return lEnr.getEnricher().getCurrentEnrichingResource();
    }

    public int getPercentageOfProcessedResources() {
        if (lEnr.getEnricher()!=null)
            return lEnr.getEnricher().getPercentageOfProcessedResources();
        else {
                System.out.println("\n\n\n\n\nlEnrInt non trovata!!!\n\n\n\n\n");
                return 10;
        }
    }

    public void doEnrichment(RDFResource protegeResource, SemanticIndex sense){
        
        lEnrInt.assertEnrichment(convertProtegeResource2OntResource(protegeResource), sense, getCurStep());
        enrichOntologyConcept(protegeResource, sense);
    }
    
    public void doEnrichment(OntResource res, SemanticIndex sense){
        lEnrInt.assertEnrichment(res, sense, getCurStep());
        enrichOntologyConcept(jenaOWLModel.getRDFResource(res), sense);
    }
    
    private void enrichOntologyConcept(RDFResource protegeResource, SemanticIndex sense) {
        LinguisticInterface linst = LinguisticWatermarkManager.getSelectedInterface();
        
        String[] terms = ((ConceptualizedLR)linst).getConceptLexicals(sense);
        Object[] parameters = {protegeResource, terms};    //forse i termini doppi andrebbero purgati ma forse ci pensa gi� ontoling

        guiFacade.notifiedFrameTermInsertion(protegeResource, terms);

        
        if (linst.hasGlosses()) {
            String gloss = ((LRWithGlosses)linst).getConceptGloss(sense);
            // arricchimento con glossa
            guiFacade.notifiedFrameGlossInsertion(protegeResource, gloss);
        }
    }
    
    public boolean canDoNextStep() {
        return lEnr.canDoNextStep();
    }
    
    public void doNextStep() {
        lEnr.doNextStep();
    }

    public void enrichAutomatic() {
        lEnr.enrichAutomatic();
    }
    
    private OntResource convertProtegeResource2OntResource(RDFResource rdfRes) {
        return m.getOntResource( rdfRes.getURI() );
    }

    private OntResource convertProtegeResource2OntResource(Frame rdfRes) {
        return m.getOntResource( ((RDFResource)rdfRes).getURI() );
    }

    
    public int getCurStep() {
        return lEnr.getCurStep();
    }
    
    public LEnrDialog getLEnrDialog(){
        return lEnr.getLEnrDialog();
    }
    
    public boolean hasStarted(){
        return lEnr.hasStarted();
    }
    
    public RDFResource nextResource(){
        RDFResource rdfres = jenaOWLModel.getRDFResource((OntResource)lEnr.nextResource());
        logger.debug("performing next resource: RdfResource: " + rdfres);
        return rdfres;
    }
    
    public RDFResource prevResource(){
        RDFResource rdfres = jenaOWLModel.getRDFResource((OntResource)lEnr.prevResource()); 
        logger.debug("performing prev resource: RdfResource: " + rdfres);
        return rdfres; 
    }

    /**
     * Sets the current Class as current frame.
     * @param cclass The Cls object to set as current frame
     */
    public void setCurResource(RDFResource res){
        lEnr.setCurResource(m.getOntResource(res.getURI()));
    }
    
    /**
     * Get the current Cls object being examined
     * @return the current Cls object
     * 
     * @uml.property name="curClass"
     */
    public RDFResource getCurResource() {
        return jenaOWLModel.getRDFResource((OntResource)lEnr.getCurResource());
    }
    
    
    public void showOptionsDlg(){
        lEnr.showOptionsDlg();
    }
    
    public LinguisticEnrichment_GUI showTableDialog(){
        return lEnr.showTableDialog();
    }
    
    /**
     * @return
     * @see it.uniroma2.art.ontoling.protege.enrichment.LinguisticEnricherProxy#getEnricher()
     */
    public LinguisticEnrichmentInterface getEnricher() {
        return lEnr.getEnricher();
    }
    
    /**
     * @return the showAS
     */
    public boolean isShowAS() {
        return lEnr.isShowAS();
    }

    /**
     * @param showAS the showAS to set
     */
    public void setShowAS(boolean showAS) {
        lEnr.setShowAS(showAS);
    }
    
    public boolean isClass(RDFResource protRes) {
        return convertProtegeResource2OntResource(protRes).isClass();
    }
    
    public boolean isProperty(RDFResource protRes) {
        return convertProtegeResource2OntResource(protRes).isProperty();
    }
 
    public boolean hasPrevResource(){
        return lEnr.hasPrevResource();
    }

    
    public boolean hasNextResource(){
        return lEnr.hasNextResource();
    }
    
    public LinkedList getSensesForFrame(RDFResource resource, int step, boolean AutomaticallySelected) {
    	logger.debug("get Sense For Frame on : " + resource + "on step " + step + "automatic select status: " + AutomaticallySelected);
        return lEnrInt.getSensesForFrame(convertProtegeResource2OntResource(resource), step, AutomaticallySelected);
    }
    
    public double getPlausibilityAtStep(RDFResource rdfRes, SemanticIndex semex, int step) {
        logger.debug("getting plausibility at Step: " + step + " for Concept-Semex Pair: " + rdfRes + ":" + semex ); 
        return lEnrInt.getPlausibilityAtStep(convertProtegeResource2OntResource(rdfRes), semex, step);
    }
    
    public boolean wasAlreadyAssigned(RDFResource rdfRes, SemanticIndex sense) {
    	logger.debug("testing wasAlreadyAssigned. rdfRes: " + rdfRes + " sense: " + sense);
        return lEnrInt.wasAlreadyAssigned(convertProtegeResource2OntResource(rdfRes), sense);
    }
    
    public boolean isEnrichable(RDFResource res) {
    	return (lEnrInt.isEnrichable(convertProtegeResource2OntResource((RDFResource)res) ) );
    }
}
