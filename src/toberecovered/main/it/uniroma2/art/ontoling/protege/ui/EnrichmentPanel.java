 /*
  * The contents of this file are subject to the Mozilla Public License
  * Version 1.1 (the "License");  you may not use this file except in compliance
  * with the License.  You may obtain a copy of the License at
  * http//www.mozilla.org/MPL/
  *
  * Software distributed under the License is distributed on an "AS IS" basis,
  * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License for
  * the specific language governing rights and limitations under the License.
  *
  * The Original Code is ONTOLING_2.0.
  *
  * The Initial Developer of the Original Code is University of Roma Tor Vergata.
  * Portions created by University of Roma Tor Vergata are Copyright (C) 2004.
  * All Rights Reserved.
  *
  * ONTOLING_2.0 was developed by the Artificial Intelligence Research Group
  * (ai-nlp.info.uniroma2.it) at the University of Roma Tor Vergata
  * Current information about ONTOLING_2.0 can be obtained at 
  * http//ai-nlp.info.uniroma2.it/software/...
  *
  */

  /*
   * Contributor(s): Armando Stellato stellato@info.uniroma2.it
  */
package it.uniroma2.art.ontoling.protege.ui;

import it.uniroma2.art.ontoling.protege.OntoLingTab;
import it.uniroma2.art.ontoling.protege.enrichment.EnrichmentTaskWrapper;
import it.uniroma2.art.ontoling.protege.enrichment.Jena2Protege2JenaLinguisticEnricherWrapper;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.ProgressMonitor;
import javax.swing.SwingConstants;
import javax.swing.Timer;

public class EnrichmentPanel extends JPanel {

    private EnrichmentPanel thisPanel;
    private JButton btnStartEnrich;
    private JButton btnCloseDB;
    private static JButton btnNextStep;
    private static Jena2Protege2JenaLinguisticEnricherWrapper lEnrWrapper;
    private ProgressMonitor progressMonitor;
    private Timer timer;
    private EnrichmentTaskWrapper enrichmentTask;
    
    EnrichmentPanel() {
    
    	
    	//lEnrWrapper = LR_Locator.getLEnrWrapper();
    	System.out.println("dentro costruttore di EnrichmentPanel"); // da cancellare
    	lEnrWrapper = new Jena2Protege2JenaLinguisticEnricherWrapper();
    	
        thisPanel = this;
        btnStartEnrich = new JButton();
        btnCloseDB = new JButton();
        btnNextStep = new JButton("Next Step");
        
        GridBagConstraints gridBagConstraints;
        
        this.setLayout(new java.awt.GridLayout(1,0,20,20));
        this.setBorder(new javax.swing.border.TitledBorder(null, "linguistic enrichment", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("MS Sans Serif", 1, 12)));
        this.setPreferredSize(new java.awt.Dimension(50, 50));
        this.setMaximumSize(new java.awt.Dimension(50, 50));
        btnStartEnrich.setPreferredSize(new java.awt.Dimension(16,25));
        btnStartEnrich.setMaximumSize(new java.awt.Dimension(16,25));
        btnStartEnrich.setHorizontalAlignment(SwingConstants.CENTER);
        btnStartEnrich.setVerticalAlignment(SwingConstants.TOP);
        btnStartEnrich.setLabel("Start");
        btnStartEnrich.setMnemonic(KeyEvent.VK_S);
        btnCloseDB.setLabel("Stop");
        btnCloseDB.setEnabled(false);
        
        btnNextStep.setEnabled(false);
        btnNextStep.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                if (lEnrWrapper.canDoNextStep()){
                   lEnrWrapper.doNextStep();
                   lEnrWrapper.getLEnrDialog().updateCurStepLabel(lEnrWrapper.getCurStep());
                   lEnrWrapper.showOptionsDlg();
                }
            }
         });
        
        // questo codice si pu� utilizzare nel caso si voglia far partire
        // lo Start come un thread separato
        /*btnStartEnrich.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                final SwingWorker worker = new SwingWorker() {
                    public Object construct() {
                        btnStartEnrich.setEnabled(false);
                        LEnr.build_plausibility_matrix();
                        btnNextStep.setEnabled(true);
                        btnNextStep.setLabel("Next Step");
                        LEnr.showOptionsDlg(); 
                        return null;
                    }
                };
                worker.start();
            }
        });*/
        
        btnStartEnrich.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                btnStartEnrich.setEnabled(false);
                
                
                enrichmentTask = new EnrichmentTaskWrapper(lEnrWrapper);
                timer = new Timer(50, new TimerListener()); //500 is time in milliseconds
                progressMonitor = new ProgressMonitor(OntoLingTab.getOntoLingTab(),
                        "Enriching Ontology Concepts",
                        "", 0, enrichmentTask.getLengthOfTask());
                progressMonitor.setProgress(0);
                progressMonitor.setMillisToDecideToPopup(10);

                enrichmentTask.go();
                timer.start();

                System.out.println("fino a qui");
                btnCloseDB.setEnabled(true);
                btnNextStep.setEnabled(true);
                btnNextStep.setLabel("Next Step"); 
            }
        });

        btnCloseDB.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e){
                btnCloseDB.setEnabled(false);
                btnNextStep.setEnabled(false);
                btnStartEnrich.setEnabled(false);
                System.out.println("Closing DB connection...");
                lEnrWrapper.getEnricher().close();
                System.out.println("done.");
            }
        });
        
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        this.add(btnStartEnrich,gridBagConstraints);
        btnNextStep.setPreferredSize(new java.awt.Dimension(16,25));
        btnNextStep.setMaximumSize(new java.awt.Dimension(16,25));
        btnNextStep.setHorizontalAlignment(SwingConstants.CENTER);
        btnNextStep.setVerticalAlignment(SwingConstants.BOTTOM);
        this.add(btnNextStep);
        this.add(btnCloseDB);
    }

    
    class TimerListener implements ActionListener {
        public void actionPerformed(ActionEvent evt) {
            progressMonitor.setProgress(enrichmentTask.getCurrent());
            String s = enrichmentTask.getMessage();
            if (s != null) {
                progressMonitor.setNote(s);
            }
            if (progressMonitor.isCanceled() || enrichmentTask.isDone()) {
                progressMonitor.close();
                enrichmentTask.stop();
                timer.stop();
            }
        }
    }
    
    
    public static Jena2Protege2JenaLinguisticEnricherWrapper getInstanceOfLinguisticEnricherWrapper() {
        return lEnrWrapper;
    }
    
    public static void disableNextStep(){
        btnNextStep.setEnabled(false);
    }
    
    public static void enableNextStep(){
        btnNextStep.setEnabled(true);
    }
    
}
